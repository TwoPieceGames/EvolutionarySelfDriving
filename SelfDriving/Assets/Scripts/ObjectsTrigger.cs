﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionHit
{
    public GameObject goOject;
    public TargetTrigger tTrigger;
    public Vector3 v3RayCastHitPosition;
    int iOctant;

    public VisionHit(GameObject  goHitObj, Vector3 v3Hit, int iOct, TargetTrigger tt = null)
    {
        goOject = goHitObj;
        v3RayCastHitPosition = v3Hit;
        iOctant = iOct;
        tTrigger = tt;
    }
}

public class ObjectsTrigger : MonoBehaviour
{
    public List<GameObject> m_goObjsInRadius;
    public List<GameObject> m_goTargets;
    public List<GameObject> m_goObstacles;

    private CarControl m_CarControl;

    public float fAngleCheck = 15;
    public float fPrecision = 2;
    public static float fCheckDistance = 25;

    public Vector3 v3Offset = new Vector3(0, 0.5f, 0);

    public Dictionary<int, VisionHit> m_igoObjectsHit = new Dictionary<int, VisionHit>();
    private List<VisionHit> m_vhObjects = new List<VisionHit>();

	// Use this for initialization
	void Start ()
    {
        m_CarControl = GetComponent<CarControl>();
    }

    VisionHit FindNearestObject()
    {
        VisionHit vhRet = null;

        float fMinDistance = 10000;
        for (int i = 0; i < m_vhObjects.Count; i++)
        {
            if ((this.transform.position - m_vhObjects[i].v3RayCastHitPosition).magnitude < fMinDistance)
            {
                fMinDistance = (this.transform.position - m_vhObjects[i].v3RayCastHitPosition).magnitude;
                vhRet = m_vhObjects[i];
            }
        }

        return vhRet;
    }

    void CheckAhead()
    {
        float fAng = -1 * Mathf.Atan2(transform.forward.x, -1 * transform.forward.z) * Mathf.Rad2Deg;
        Vector3 v3Ang = Vector3.zero;
        v3Ang.x = -Mathf.Sin(Mathf.Deg2Rad * fAng);
        v3Ang.z = -Mathf.Cos(Mathf.Deg2Rad * fAng);

        RaycastHit hitInfo;

        float fActiveAngle = fAng;
        while (fActiveAngle < fAng + fAngleCheck)
        {
            fActiveAngle += fPrecision;
            v3Ang.x = -Mathf.Sin(Mathf.Deg2Rad * fActiveAngle);
            v3Ang.z = -Mathf.Cos(Mathf.Deg2Rad * fActiveAngle);
            v3Ang.y = 0;
            Debug.DrawRay(this.transform.position + v3Offset, v3Ang * fCheckDistance, Color.green);
            if (Physics.Raycast(this.transform.position + v3Offset, v3Ang * fCheckDistance, out hitInfo))
            {
                //print("Hitting: " + hitInfo.collider.name);
                VisionHit vh = new VisionHit(hitInfo.collider.gameObject, hitInfo.point, 1, null);
                m_vhObjects.Add(vh);
            }
        }

        m_igoObjectsHit.Add(1, FindNearestObject());
        m_vhObjects.Clear();

        fActiveAngle = fAng;
        while (fActiveAngle > fAng - fAngleCheck)
        {
            fActiveAngle -= fPrecision;
            v3Ang.x = -Mathf.Sin(Mathf.Deg2Rad * fActiveAngle);
            v3Ang.z = -Mathf.Cos(Mathf.Deg2Rad * fActiveAngle);
            v3Ang.y = 0;
            Debug.DrawRay(this.transform.position + v3Offset, v3Ang * fCheckDistance, Color.red);
            if (Physics.Raycast(this.transform.position + v3Offset, v3Ang * fCheckDistance, out hitInfo))
            {
                //print("Hitting: " + hitInfo.collider.name);
                VisionHit vh = new VisionHit(hitInfo.collider.gameObject, hitInfo.point, 8, null);
                m_vhObjects.Add(vh);
            }
        }

        m_igoObjectsHit.Add(8, FindNearestObject());
        m_vhObjects.Clear();
    }

    void CheckSides()
    {
        Vector3 v3Inter = (transform.forward + transform.right).normalized;
        float fAng = -1 * Mathf.Atan2(v3Inter.x, -1 * v3Inter.z) * Mathf.Rad2Deg;
        Vector3 v3Ang = Vector3.zero;
        v3Ang.x = -Mathf.Sin(Mathf.Deg2Rad * fAng);
        v3Ang.z = -Mathf.Cos(Mathf.Deg2Rad * fAng);

        RaycastHit hitInfo;

        float fActiveAngle = fAng;
        while (fActiveAngle < fAng + fAngleCheck)
        {
            fActiveAngle += fPrecision;
            v3Ang.x = -Mathf.Sin(Mathf.Deg2Rad * fActiveAngle);
            v3Ang.z = -Mathf.Cos(Mathf.Deg2Rad * fActiveAngle);
            v3Ang.y = 0;
            Debug.DrawRay(this.transform.position + v3Offset, v3Ang * fCheckDistance, Color.blue);
            if (Physics.Raycast(this.transform.position + v3Offset, v3Ang * fCheckDistance, out hitInfo))
            {
                //print("Hitting: " + hitInfo.collider.name);
                VisionHit vh = new VisionHit(hitInfo.collider.gameObject, hitInfo.point, 1, null);
                m_vhObjects.Add(vh);
            }
        }

        m_igoObjectsHit.Add(3, FindNearestObject());
        m_vhObjects.Clear();

        fActiveAngle = fAng;
        while (fActiveAngle > fAng - fAngleCheck)
        {
            fActiveAngle -= fPrecision;
            v3Ang.x = -Mathf.Sin(Mathf.Deg2Rad * fActiveAngle);
            v3Ang.z = -Mathf.Cos(Mathf.Deg2Rad * fActiveAngle);
            v3Ang.y = 0;
            Debug.DrawRay(this.transform.position + v3Offset, v3Ang * fCheckDistance, Color.yellow);
            if (Physics.Raycast(this.transform.position + v3Offset, v3Ang * fCheckDistance, out hitInfo))
            {
                //print("Hitting: " + hitInfo.collider.name);
                VisionHit vh = new VisionHit(hitInfo.collider.gameObject, hitInfo.point, 1, null);
                m_vhObjects.Add(vh);
            }
        }

        m_igoObjectsHit.Add(2, FindNearestObject());
        m_vhObjects.Clear();

        v3Inter = (transform.forward + -transform.right).normalized;
        fAng = -1 * Mathf.Atan2(v3Inter.x, -1 * v3Inter.z) * Mathf.Rad2Deg;
        v3Ang = Vector3.zero;
        v3Ang.x = -Mathf.Sin(Mathf.Deg2Rad * fAng);
        v3Ang.z = -Mathf.Cos(Mathf.Deg2Rad * fAng);

        //left side

        fActiveAngle = fAng;
        while (fActiveAngle < fAng + fAngleCheck)
        {
            fActiveAngle += fPrecision;
            v3Ang.x = -Mathf.Sin(Mathf.Deg2Rad * fActiveAngle);
            v3Ang.z = -Mathf.Cos(Mathf.Deg2Rad * fActiveAngle);
            v3Ang.y = 0;
            Debug.DrawRay(this.transform.position + v3Offset, v3Ang * fCheckDistance, Color.blue);
            if (Physics.Raycast(this.transform.position + v3Offset, v3Ang * fCheckDistance, out hitInfo))
            {
                //print("Hitting: " + hitInfo.collider.name);
                VisionHit vh = new VisionHit(hitInfo.collider.gameObject, hitInfo.point, 1, null);
                m_vhObjects.Add(vh);
            }
        }

        m_igoObjectsHit.Add(7, FindNearestObject());
        m_vhObjects.Clear();

        fActiveAngle = fAng;
        while (fActiveAngle > fAng - fAngleCheck)
        {
            fActiveAngle -= fPrecision;
            v3Ang.x = -Mathf.Sin(Mathf.Deg2Rad * fActiveAngle);
            v3Ang.z = -Mathf.Cos(Mathf.Deg2Rad * fActiveAngle);
            v3Ang.y = 0;
            Debug.DrawRay(this.transform.position + v3Offset, v3Ang * fCheckDistance, Color.yellow);
            if (Physics.Raycast(this.transform.position + v3Offset, v3Ang * fCheckDistance, out hitInfo))
            {
                //print("Hitting: " + hitInfo.collider.name);
                VisionHit vh = new VisionHit(hitInfo.collider.gameObject, hitInfo.point, 1, null);
                m_vhObjects.Add(vh);
            }
        }

        m_igoObjectsHit.Add(6, FindNearestObject());
        m_vhObjects.Clear();
    }

    void CheckBehind()
    {
        float fAng = -1 * Mathf.Atan2(-transform.forward.x, -1 * -transform.forward.z) * Mathf.Rad2Deg;
        Vector3 v3Ang = Vector3.zero;
        v3Ang.x = -Mathf.Sin(Mathf.Deg2Rad * fAng);
        v3Ang.z = -Mathf.Cos(Mathf.Deg2Rad * fAng);

        RaycastHit hitInfo;

        float fActiveAngle = fAng;
        while (fActiveAngle < fAng + fAngleCheck)
        {
            fActiveAngle += fPrecision;
            v3Ang.x = -Mathf.Sin(Mathf.Deg2Rad * fActiveAngle);
            v3Ang.z = -Mathf.Cos(Mathf.Deg2Rad * fActiveAngle);
            v3Ang.y = 0;
            Debug.DrawRay(this.transform.position + v3Offset, v3Ang * fCheckDistance, Color.green);
            if (Physics.Raycast(this.transform.position + v3Offset, v3Ang * fCheckDistance, out hitInfo))
            {
                //print("Hitting: " + hitInfo.collider.name);
                VisionHit vh = new VisionHit(hitInfo.collider.gameObject, hitInfo.point, 1, null);
                m_vhObjects.Add(vh);
            }
        }

        m_igoObjectsHit.Add(5, FindNearestObject());
        m_vhObjects.Clear();

        fActiveAngle = fAng;
        while (fActiveAngle > fAng - fAngleCheck)
        {
            fActiveAngle -= fPrecision;
            v3Ang.x = -Mathf.Sin(Mathf.Deg2Rad * fActiveAngle);
            v3Ang.z = -Mathf.Cos(Mathf.Deg2Rad * fActiveAngle);
            v3Ang.y = 0;
            Debug.DrawRay(this.transform.position + v3Offset, v3Ang * fCheckDistance, Color.red);
            if (Physics.Raycast(this.transform.position + v3Offset, v3Ang * fCheckDistance, out hitInfo))
            {
                //print("Hitting: " + hitInfo.collider.name);
                VisionHit vh = new VisionHit(hitInfo.collider.gameObject, hitInfo.point, 1, null);
                m_vhObjects.Add(vh);
            }
        }

        m_igoObjectsHit.Add(4, FindNearestObject());
        m_vhObjects.Clear();
    }
	
	// Update is called once per frame
	void Update ()
    {
        m_igoObjectsHit.Clear();
        CheckAhead();
        CheckSides();
        CheckBehind();
        //Debug.DrawRay(this.transform.position, transform.forward * 10, Color.green);	
    }

    private void OnTriggerEnter(Collider other)
    {
        GameObject goBase = other.transform.root.gameObject;
        if (!m_goObjsInRadius.Contains(goBase))
        {
            m_goObjsInRadius.Add(goBase);
            if (goBase.GetComponent<TargetTrigger>())
            {
                m_goTargets.Add(goBase);
            }
            else
            {
                m_goObstacles.Add(goBase);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (m_goObjsInRadius.Contains(other.transform.root.gameObject))
        {
            m_goObjsInRadius.Remove(other.transform.root.gameObject);
        }

        if (m_goTargets.Contains(other.transform.root.gameObject))
        {
            m_goTargets.Remove(other.transform.root.gameObject);
        }

        if (m_goObstacles.Contains(other.transform.root.gameObject))
        {
            m_goObstacles.Remove(other.transform.root.gameObject);
        }
    }
}
