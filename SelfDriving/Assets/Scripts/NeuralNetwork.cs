﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeuralNetwork
{
    protected CarControl m_CarControl;

    public float[][][] m_Weights;
    protected float[] m_LayerBiases;
    protected float[] m_CurrentLayerValues;
    protected float[] m_NextLayerValues;

    public float[][] m_RecurrentWeights;

    protected static float fSigmoidHigh = NeuralNetworkUtils.MinMax(NeuralNetworkUtils.Sigmoid(4), 1, 0, 1, -1);
    protected static float fSigmoidLow = NeuralNetworkUtils.MinMax(NeuralNetworkUtils.Sigmoid(-4), 1, 0, 1, -1);

    protected static float iMinButtonPressThreshold = 4.5f;

    protected int m_iGeneration;
    protected int m_iSpecies;
    protected float m_fDiversity;
    protected float m_fAvgFitness = 0;
    protected float m_fPeakFitness = 0;
    protected float m_fFitness;
    protected int m_iAverageCount = 0;

    protected float fPassiveFitnessGain = 0.005f;
    protected float fPassiveFitness = 0;
    protected float fDamageFitnessFactor = 5f;
    protected float fStockValue = 120;
    protected float fMovementFitnessFactor = 0.05f;

    private static int iInputLayerSize = 19;

    private static int iVelocityXIdx = 0;
    private static int iVelocityZIdx = 1;
    private static int iQuadrant1Distance = 3;
    private static int iQuadrant2Distance = 4;
    private static int iQuadrant3Distance = 5;
    private static int iQuadrant4Distance = 6;
    private static int iQuadrant5Distance = 7;
    private static int iQuadrant6Distance = 8;
    private static int iQuadrant7Distance = 9;
    private static int iQuadrant8Distance = 10;

    private static int iQuadrant1TargetOrObsIdx = 11;
    private static int iQuadrant2TargetOrObsIdx = 12;
    private static int iQuadrant3TargetOrObsIdx = 13;
    private static int iQuadrant4TargetOrObsIdx = 14;
    private static int iQuadrant5TargetOrObsIdx = 15;
    private static int iQuadrant6TargetOrObsIdx = 16;
    private static int iQuadrant7TargetOrObsIdx = 17;
    private static int iQuadrant8TargetOrObsIdx = 18;

    public static float fVelocityMultiplier = 0.08f;
    public static float fDistanceMultiplier = 1/25f;

    private float[] m_Inputs;
    private float[] m_Outputs;
    private static int iNumHiddenLayers = 1;
    private static int[] iHiddenLayerSizes = { 12, 16, 14, 12, 10 };
    private static int iNumOutputs = 4;

    private int iTrainingCount = 0;
    private int iTrainingInterval = 60 * 5;

    private float[] m_HiddenLayerValues;
    private float[] m_CurrentHiddenLayerValues;

    private List<float[]> m_PreviousHiddenValues;

    private List<float[]> m_PreviousCellState;
    private List<float[][]> m_HiddenLayerDeltas;
    private List<float> m_Deltas;

    private int m_iCurrentFrameDifference = 0;
    private int m_iMaxPreviousTimesteps = 6;

    private static int m_iMaxFrameUpdateTime = 6;

    List<TrainingSet> m_TrainingSets;

    List<float[]> m_previousHiddenLayerValues;



    public float m_fLearningRate = 2; // default
    private float fMinFitnessDif = 15;

    public int GetGeneration() { return m_iGeneration; }
    public int GetSpecies() { return m_iSpecies; }
    public void SetSpecies(int m_iSpec) { m_iSpecies = m_iSpec; }
    public float GetDiversity() { return m_fDiversity; }
    public float GetPeakFitness() { return m_fPeakFitness; }

    public void SetPeakFitness(float val) { m_fPeakFitness = val; }
    public float GetAverageFitness() { return m_fAvgFitness; }

    public float GetFitness() { return m_fFitness; }

    public float[] GetOutputs() {  return m_Outputs; }

    public NeuralNetwork(int generation, int species, CarControl cc, int maxsteps, int timegap)
    {
        m_iGeneration = generation;
        m_iSpecies = species;
        m_HiddenLayerValues = new float[iHiddenLayerSizes[0]];
        m_PreviousHiddenValues = new List<float[]>();
        m_previousHiddenLayerValues = new List<float[]>();
        m_CarControl = cc;

        m_LayerBiases = new float[1 + iNumHiddenLayers];
        for (int i = 0; i < m_LayerBiases.Length; i++)
        {
            m_LayerBiases[i] = 1;
        }
        m_HiddenLayerDeltas = new List<float[][]>();
        m_TrainingSets = new List<TrainingSet>();

        m_iMaxPreviousTimesteps = maxsteps;
        m_iMaxFrameUpdateTime = timegap;

    }

    public void SetDiversity(ref float[][][] parentWeights)
    {
        for (int i = 0; i < m_Weights.Length; i++)
        {
            for (int j = 0; j < m_Weights[i].Length; j++)
            {
                for (int k = 0; k < m_Weights[i][j].Length; k++)
                {
                    if (Mathf.Abs(m_Weights[i][j][k] - parentWeights[i][j][k]) > 0.0001f)
                    {
                        m_fDiversity += 1;
                    }
                }
            }
        }
    }

    public void SetLearningRate(float fVal)
    {
        m_fLearningRate = fVal;
    }

    public void SetTrainingInterval(float fSeconds)
    {
        iTrainingInterval = (int)(60 * fSeconds);
    }

    public void SetMinFitnessDifferential(float fValue)
    {
        fMinFitnessDif = fPassiveFitnessGain * iTrainingInterval + fValue;
    }

    public void MutateWeights(float fMutationRate, float fStepSize /* = 1 - 4*/)
    {
        //seed
        UnityEngine.Random.seed = (int)System.DateTime.Now.Ticks;
        for (int i = 0; i < m_Weights.Length; i++)
        {
            for (int j = 0; j < m_Weights[i].Length; j++)
            {
                for (int k = 0; k < m_Weights[i][j].Length; k++)
                {
                    float fRand = UnityEngine.Random.Range(0f, 1f);
                    if (fRand < fMutationRate)
                    {
                        float z = UnityEngine.Random.Range(-1f, 1f);
                        m_Weights[i][j][k] = Mathf.Lerp(m_Weights[i][j][k], z, fStepSize);
                    
                    }
                }
            }
        }
    }

    public virtual void InitWeights(NetworkStorageData lNSD = null, string sFilename = "", bool bPerserveGenerationAndSpecies = false)
    {
        if (lNSD == null && sFilename == "")
        {
            UnityEngine.Random.InitState((int)System.DateTime.Now.Ticks);

            // Init Weight Arrays
            m_Weights = new float[iNumHiddenLayers + 1][][];
            for (int i = 0; i < iNumHiddenLayers + 1; i++)
            {
                if (i == 0)
                {
                    // input -> first hidden layer
                    m_Weights[i] = new float[iInputLayerSize][];
                    for (int j = 0; j < iInputLayerSize; j++)
                    {
                        m_Weights[i][j] = new float[iHiddenLayerSizes[0]];
                    }
                }
                else if (i < iNumHiddenLayers)
                {
                    m_Weights[i] = new float[iHiddenLayerSizes[i - 1]][];
                    for (int j = 0; j < iHiddenLayerSizes[i - 1]; j++)
                    {
                        m_Weights[i][j] = new float[iHiddenLayerSizes[i]];
                    }
                }
                else
                {
                    m_Weights[i] = new float[iHiddenLayerSizes[i - 1]][];
                    for (int j = 0; j < iHiddenLayerSizes[i - 1]; j++)
                    {
                        m_Weights[i][j] = new float[iNumOutputs];
                    }
                }
            }

            // Insert Random Values Into Weights
            for (int i = 0; i < m_Weights.Length; i++)
            {
                for (int j = 0; j < m_Weights[i].Length; j++)
                {
                    for (int k = 0; k < m_Weights[i][j].Length; k++)
                    {
                        float fRand = UnityEngine.Random.Range(-1f, 1f);
                        m_Weights[i][j][k] = fRand;
                    }
                }
            }
        }
        else if (sFilename != "")
        {
            //unpack
            List<NetworkStorageData> nNetList = NeuralNetworkUtils.ReadLinesIntoObjects(sFilename);
            if (!bPerserveGenerationAndSpecies)
            {
                m_iGeneration = nNetList[0].iGeneration;
                m_iSpecies = nNetList[0].iSpecies;
            }
            m_Weights = NeuralNetworkUtils.ConvertStorageWeights(nNetList[0].fWeights);
        }


        WriteData();
    }

    protected void UpdateFitness()
    {
        m_fFitness = m_CarControl.transform.position.magnitude;

        fPassiveFitness += fPassiveFitnessGain;
        m_fFitness += fPassiveFitness;

        m_fFitness += m_CarControl.iRewards;
        

        m_fAvgFitness = (m_fAvgFitness * m_iAverageCount + m_fFitness)
            / (++m_iAverageCount);
        if (m_fFitness > m_fPeakFitness)
        {
            m_fPeakFitness = m_fFitness;
        }

    }

    private void Normalize(CarControl cc, ref float[] inputWeights)
    {
        inputWeights[iVelocityXIdx] = cc.v3MoveDirection.x * fVelocityMultiplier;
        inputWeights[iVelocityZIdx] = cc.v3MoveDirection.z * fVelocityMultiplier;

        VisionHit vh = cc.GetQuadrantObject(1);
        if (vh != null)
        {
            inputWeights[iQuadrant1Distance] = ((cc.transform.position - vh.v3RayCastHitPosition).magnitude) * fDistanceMultiplier;
            if (vh.goOject.GetComponent<TargetTrigger>())
            {
                inputWeights[iQuadrant1TargetOrObsIdx] = 1;
            }
            else
            {
                inputWeights[iQuadrant1TargetOrObsIdx] = -1;
            }
        }
        else
        {
            inputWeights[iQuadrant1Distance] = 0; //max
            inputWeights[iQuadrant1TargetOrObsIdx] = 0;
        }

        vh = cc.GetQuadrantObject(2);
        if (vh != null)
        {
            inputWeights[iQuadrant2Distance] = ((cc.transform.position - vh.v3RayCastHitPosition).magnitude) * fDistanceMultiplier;
            if (vh.goOject.GetComponent<TargetTrigger>())
            {
                inputWeights[iQuadrant2TargetOrObsIdx] = 1;
            }
            else
            {
                inputWeights[iQuadrant2TargetOrObsIdx] = -1;
            }
        }
        else
        {
            inputWeights[iQuadrant2Distance] = 0; //max
            inputWeights[iQuadrant2TargetOrObsIdx] = 0;
        }

        vh = cc.GetQuadrantObject(3);
        if (vh != null)
        {
            inputWeights[iQuadrant3Distance] = ((cc.transform.position - vh.v3RayCastHitPosition).magnitude) * fDistanceMultiplier;
            if (vh.goOject.GetComponent<TargetTrigger>())
            {
                inputWeights[iQuadrant3TargetOrObsIdx] = 1;
            }
            else
            {
                inputWeights[iQuadrant3TargetOrObsIdx] = -1;
            }
        }
        else
        {
            inputWeights[iQuadrant3Distance] = 0; //max
            inputWeights[iQuadrant3TargetOrObsIdx] = 0;
        }

        vh = cc.GetQuadrantObject(4);
        if (vh != null)
        {
            inputWeights[iQuadrant4Distance] = ((cc.transform.position - vh.v3RayCastHitPosition).magnitude) * fDistanceMultiplier;
            if (vh.goOject.GetComponent<TargetTrigger>())
            {
                inputWeights[iQuadrant4TargetOrObsIdx] = 1;
            }
            else
            {
                inputWeights[iQuadrant4TargetOrObsIdx] = -1;
            }
        }
        else
        {
            inputWeights[iQuadrant4Distance] = 0; //max
            inputWeights[iQuadrant4TargetOrObsIdx] = 0;
        }

        vh = cc.GetQuadrantObject(5);
        if (vh != null)
        {
            inputWeights[iQuadrant5Distance] = ((cc.transform.position - vh.v3RayCastHitPosition).magnitude) * fDistanceMultiplier;
            if (vh.goOject.GetComponent<TargetTrigger>())
            {
                inputWeights[iQuadrant5TargetOrObsIdx] = 1;
            }
            else
            {
                inputWeights[iQuadrant5TargetOrObsIdx] = -1;
            }
        }
        else
        {
            inputWeights[iQuadrant5Distance] = 0; //max
            inputWeights[iQuadrant5TargetOrObsIdx] = 0;
        }

        vh = cc.GetQuadrantObject(6);
        if (vh != null)
        {
            inputWeights[iQuadrant6Distance] = ((cc.transform.position - vh.v3RayCastHitPosition).magnitude) * fDistanceMultiplier;
            if (vh.goOject.GetComponent<TargetTrigger>())
            {
                inputWeights[iQuadrant6TargetOrObsIdx] = 1;
            }
            else
            {
                inputWeights[iQuadrant6TargetOrObsIdx] = -1;
            }
        }
        else
        {
            inputWeights[iQuadrant6Distance] = 0; //max
            inputWeights[iQuadrant6TargetOrObsIdx] = 0;
        }

        vh = cc.GetQuadrantObject(7);
        if (vh != null)
        {
            inputWeights[iQuadrant7Distance] = ((cc.transform.position - vh.v3RayCastHitPosition).magnitude) * fDistanceMultiplier;
            if (vh.goOject.GetComponent<TargetTrigger>())
            {
                inputWeights[iQuadrant7TargetOrObsIdx] = 1;
            }
            else
            {
                inputWeights[iQuadrant7TargetOrObsIdx] = -1;
            }
        }
        else
        {
            inputWeights[iQuadrant7Distance] = 0; //max
            inputWeights[iQuadrant7TargetOrObsIdx] = 0;
        }

        vh = cc.GetQuadrantObject(8);
        if (vh != null)
        {
            inputWeights[iQuadrant8Distance] = ((cc.transform.position - vh.v3RayCastHitPosition).magnitude) * fDistanceMultiplier;
            if (vh.goOject.GetComponent<TargetTrigger>())
            {
                inputWeights[iQuadrant8TargetOrObsIdx] = 1;
            }
            else
            {
                inputWeights[iQuadrant8TargetOrObsIdx] = -1;
            }
        }
        else
        {
            inputWeights[iQuadrant8Distance] = 0; //max
            inputWeights[iQuadrant8TargetOrObsIdx] = 0;
        }

        m_Inputs = inputWeights;
    }

    public void Forward()
    {
        iTrainingCount += 1;
        m_CurrentLayerValues = new float[iInputLayerSize];
        //pass inputs 
        Normalize(m_CarControl, ref m_CurrentLayerValues);
        //Normalize(m_Move, ref m_Inputs, enemyMv);

        //input layer to first hidden layer
        CalculateNextLayerValue(0, iInputLayerSize);
        CalculateHiddenLayerValues();
        CalculateOutputLayer();
        UpdateFitness();

        //store the output weights, and current fitness in the storage array so we can later come back and train based on the result of this
    }

    private void CalculateNextLayerValue(int iLayer, int iCurrentLayerSize)
    {
        m_HiddenLayerValues = new float[iHiddenLayerSizes[0]];
        for (int iNextIndex = 0; iNextIndex < iHiddenLayerSizes[iLayer]; iNextIndex++)
        {
            float fSum = 0;
            for (int i = 0; i < iCurrentLayerSize; i++)
            {
                fSum += m_CurrentLayerValues[i] * m_Weights[iLayer][i][iNextIndex];
            }
            //fSum += m_LayerBiases[iLayer];
            m_HiddenLayerValues[iNextIndex] = NeuralNetworkUtils.Sigmoid(fSum);
        }
        m_CurrentLayerValues = m_HiddenLayerValues;
    }

    private void CalculateHiddenLayerValues()
    {
        for (int l = 1; l < iNumHiddenLayers; l++)
        {
            m_HiddenLayerValues = new float[iHiddenLayerSizes[l]];
            for (int iNextIndex = 0; iNextIndex < iHiddenLayerSizes[l]; iNextIndex++)
            {
                float fSum = 0;
                for (int i = 0; i < iHiddenLayerSizes[l-1]; i++)
                {
                    fSum += m_CurrentLayerValues[i] * m_Weights[l][i][iNextIndex];
                }
                //fSum += m_LayerBiases[iLayer];
                m_HiddenLayerValues[iNextIndex] = NeuralNetworkUtils.Sigmoid(fSum);
            }
            m_CurrentLayerValues = m_HiddenLayerValues;
        }
       
    }

    private void CalculateOutputLayer()
    {
        m_NextLayerValues = new float[iNumOutputs];

        // Insert the next values, if we have exceeded the max previous timesteps, 
        // AND the appropriate number of frames has passed delete our old data.

        float[] newHiddenValues = new float[iHiddenLayerSizes[0]];
        m_CurrentLayerValues.CopyTo(newHiddenValues, 0);
        m_PreviousHiddenValues.Insert(0, newHiddenValues);
        m_iCurrentFrameDifference = 0;

        if (m_PreviousHiddenValues.Count > m_iMaxPreviousTimesteps)
        {
            m_PreviousHiddenValues.RemoveAt(m_PreviousHiddenValues.Count - 1);
        }


        for (int i = 0; i < iNumOutputs; i++)
        {
            float fSum = 0;
            for (int j = 0; j < iHiddenLayerSizes[iHiddenLayerSizes.Length - 1]; j++)
            {
                fSum += m_HiddenLayerValues[j] * m_Weights[m_Weights.Length - 1][j][i];
            }
            fSum += m_LayerBiases[m_Weights.Length - 1];
            m_NextLayerValues[i] = NeuralNetworkUtils.Sigmoid(fSum);
        }

        
        for (int i = 0; i < m_NextLayerValues.Length; i++)
        {
            m_NextLayerValues[i] = NeuralNetworkUtils.MinMax(m_NextLayerValues[i], 1, 0, 10, -10);
        }
        
        m_CurrentLayerValues = m_NextLayerValues;

        //two seperate softmaxes, one for the controller axes, and one for button pressed
        float[] fAxes = new float[8];
        float[] fButtons = new float[6];

        m_Outputs = m_CurrentLayerValues;
    }

    public void SetInputs(ref List<string> sButtons)
    {

        sButtons.Clear();
        if (m_CurrentLayerValues[0] > iMinButtonPressThreshold)
        {
            sButtons.Add("w");
        }

        if (m_CurrentLayerValues[1] > iMinButtonPressThreshold)
        {
            sButtons.Add("a");
        }

        if (m_CurrentLayerValues[2] > iMinButtonPressThreshold)
        {
            sButtons.Add("d");
        }

        if (m_CurrentLayerValues[3] > iMinButtonPressThreshold)
        {
            sButtons.Add("s");
        }
    }


    public void BackPropegate(float[] fPrediction)
    {

    }

    public void WriteData(string sPath = "", float[][] recurrentWeights = null)
    {
        if (sPath == "")
        {
            sPath = "C:\\Programs\\SelfDriving\\NetworkWeights\\Car\\" + m_iGeneration.ToString();
        }
        System.IO.Directory.CreateDirectory(sPath);
        sPath += "\\" + "NetworkWeights" + m_iSpecies.ToString() + ".txt";
        NeuralNetworkUtils.WriteWeights(sPath, m_Weights, recurrentWeights, m_iGeneration, m_iSpecies, UnityEngine.Random.seed);
    }

}

public class TrainingSet
{
    float m_fFitness;
    float[] m_fOutputs;
    float[][] m_fHiddenLayerValues;
    float[] m_fInputs;

    public TrainingSet(float fFitness, float[] fOut, float[] fInputs)
    {
        m_fFitness = fFitness;
        m_fOutputs = new float[fOut.Length];
        m_fInputs = new float[fInputs.Length];


        fOut.CopyTo(m_fOutputs, 0);

        fInputs.CopyTo(m_fInputs, 0);

    }

    public float GetFitnessDif()
    {
        return m_fFitness;
    }

    public float[] GetOutputs()
    {
        return m_fOutputs;
    }

    public float[] GetInputs()
    {
        return m_fInputs;
    }

    public float[][] GetHiddenLayerValues()
    {
        return m_fHiddenLayerValues;
    }
}
