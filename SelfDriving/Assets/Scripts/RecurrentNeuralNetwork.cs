﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecurrentNeuralNetwork : NeuralNetwork
{
    private static int iInputLayerSize = 25;

    private static int iPositionXIdx = 0;
    private static int iPositionZIdx = 1;
    private static int iVelocityXIdx = 2;
    private static int iVelocityZIdx = 3;
    private static int iQuadrant1Distance = 4;
    private static int iQuadrant2Distance = 5;
    private static int iQuadrant3Distance = 6;
    private static int iQuadrant4Distance = 8;
    private static int iQuadrant5Distance = 10;
    private static int iQuadrant6Distance = 11;
    private static int iQuadrant7Distance = 12;
    private static int iQuadrant8Distance = 13;

    private static int iQuadrant1TargetOrObsIdx = 14;
    private static int iQuadrant2TargetOrObsIdx = 15;
    private static int iQuadrant3TargetOrObsIdx = 16;
    private static int iQuadrant4TargetOrObsIdx = 18;
    private static int iQuadrant5TargetOrObsIdx = 20;
    private static int iQuadrant6TargetOrObsIdx = 22;
    private static int iQuadrant7TargetOrObsIdx = 23;
    private static int iQuadrant8TargetOrObsIdx = 24;


    private static int iNumHiddenLayers = 1;
    private static int[] iHiddenLayerSizes = { 18 };
    private static int iNumOutputs = 4;

    public static float fVelocityMultiplier = 0.08f;


    public float[][] m_RecurrentWeights;
    public float[][] m_ResetWeights;
    public float[][] m_UpdateWeights;

    private float[] m_HiddenLayerValues;
    private float[] m_CurrentHiddenLayerValues;

    private List<float[]> m_PreviousHiddenValues;
    private List<float[]> m_PreviousCellState;
    private List<float[][]> m_HiddenLayerDeltas;
    private List<float> m_Deltas;

    private float[] m_Outputs;

    private int m_iMaxPreviousTimesteps = 6;

    private int m_iCurrentFrameDifference = 0;
    private static int m_iMaxFrameUpdateTime = 6;

    JsonSerializer serializer = new JsonSerializer();

    List<float[]> m_previousHiddenLayerValues;
    //use these to look up when doing back-propegation, the outputs and fitness I timesteps back
    float[] m_previousLearningTimestepOutput; /*iMaxTimeSteps * iMaxFrameStep [36] frames back*/
                                              // non-sigmoided stored values taken every 36 frames, so our learning algorithm is doing math properly
    List<float[]> m_previousLearningTimestepHiddenValues;
    List<float> m_previousTimestepFitness;
    float m_previousFitnessTimestep;

    int iMaxTimeSteps = 6;
    int iFrame = 0;
    static int iMaxFrameStep = 6; //equates to looking back 36 frames max

    private static float m_fCorrectionProbability = 0.5f; //we have a 50% chance to correct each output node based on result
    private static float m_fCorrectionIncreasePercentage = 1.25f;

    private int iCurrentLayer = 0;
    private float[] m_Inputs;
    private float[] m_fActual;

    List<TrainingSet> m_TrainingSets;

    public bool bTrain;
    public float m_fLearningRate = 2; // default

    private int iTrainingCount = 0;
    private int iTrainingInterval = 60 * 5;
    private float fMinFitnessDif = 15;

    private int iTrainingIterationCount = 0;

    public float[] GetCurrentOutputs()
    {
        return m_Outputs;
    }

    public void SetRecurrentWeights(float[][] recurrentWeights)
    {
        m_RecurrentWeights = recurrentWeights;
    }

    public void SetResetWeights(float[][] resetWeights)
    {
        m_ResetWeights = resetWeights;
    }

    public void SetUpdateWeights(float[][] updateWeights)
    {
        m_UpdateWeights = updateWeights;
    }

    public void SetTrainingInterval(float fSeconds)
    {
        iTrainingInterval = (int)(60 * fSeconds);
    }

    public void SetMinFitnessDifferential(float fValue)
    {
        fMinFitnessDif = fPassiveFitnessGain * iTrainingInterval + fValue;
    }

    public void SetLearningRate(float fVal)
    {
        m_fLearningRate = fVal;
    }

    public void MutateRecurrentWeights(float fMutationRate, float fStepSize /* = 1 - 4*/)
    {
        //seed
        UnityEngine.Random.seed = Guid.NewGuid().GetHashCode();
        for (int i = 0; i < m_RecurrentWeights.Length; i++)
        {
            for (int j = 0; j < m_RecurrentWeights[i].Length; j++)
            {
                float fRand = UnityEngine.Random.Range(0f, 1f);
                if (fRand < fMutationRate)
                {
                    int z = UnityEngine.Random.Range(0, 2);
                    if (z == 0)
                    {
                        m_RecurrentWeights[i][j] = (m_RecurrentWeights[i][j] + -1) * fStepSize;
                    }
                    else
                    {
                        m_RecurrentWeights[i][j] = (m_RecurrentWeights[i][j] + 1) * fStepSize;
                    }
                }
            }
        }
    }

    public void MutateResetWeights(float fMutationRate, float fStepSize)
    {
        //seed
        UnityEngine.Random.seed = Guid.NewGuid().GetHashCode();
        for (int i = 0; i < m_ResetWeights.Length; i++)
        {
            for (int j = 0; j < m_ResetWeights[i].Length; j++)
            {
                float fRand = UnityEngine.Random.Range(0f, 1f);
                if (fRand < fMutationRate)
                {
                    int z = UnityEngine.Random.Range(0, 2);
                    if (z == 0)
                    {
                        m_ResetWeights[i][j] = (m_ResetWeights[i][j] + -1) * fStepSize;
                    }
                    else
                    {
                        m_ResetWeights[i][j] = (m_ResetWeights[i][j] + 1) * fStepSize;
                    }
                }
            }
        }
    }

    public void MutateUpdateWeights(float fMutationRate, float fStepSize)
    {
        //seed
        UnityEngine.Random.seed = (int)System.DateTime.Now.Ticks;
        for (int i = 0; i < m_UpdateWeights.Length; i++)
        {
            for (int j = 0; j < m_UpdateWeights[i].Length; j++)
            {
                float fRand = UnityEngine.Random.Range(0f, 1f);
                if (fRand < fMutationRate)
                {
                    int z = UnityEngine.Random.Range(0, 2);
                    if (z == 0)
                    {
                        m_UpdateWeights[i][j] = (m_UpdateWeights[i][j] + -1) * fStepSize;
                    }
                    else
                    {
                        m_UpdateWeights[i][j] = (m_UpdateWeights[i][j] + 1) * fStepSize;
                    }
                }
            }
        }
    }

    public RecurrentNeuralNetwork(int generation, int species, CarControl cc, int maxsteps, int timegap) 
        : base(generation, species, cc, maxsteps, timegap)
    {
        m_iGeneration = generation;
        m_iSpecies = species;
        m_HiddenLayerValues = new float[iHiddenLayerSizes[0]];
        m_PreviousHiddenValues = new List<float[]>();
        m_PreviousCellState = new List<float[]>();
        m_previousHiddenLayerValues = new List<float[]>();
        m_CarControl = cc;

        m_LayerBiases = new float[2];
        for (int i = 0; i < m_LayerBiases.Length; i++)
        {
            m_LayerBiases[i] = 1;
        }
        m_HiddenLayerDeltas = new List<float[][]>();
        m_TrainingSets = new List<TrainingSet>();

        m_iMaxPreviousTimesteps = maxsteps;
        m_iMaxFrameUpdateTime = timegap;

    }

    public override void InitWeights(NetworkStorageData lNSD = null, string sFilename = "", bool bPerserveGenerationAndSpecies = false)
    {
        if (lNSD == null && sFilename == "")
        {
            UnityEngine.Random.InitState((int)System.DateTime.Now.Ticks);

            // Init Weight Arrays
            m_Weights = new float[2][][];
            for (int i = 0; i < 2; i++)
            {
                if (i == 0)
                {
                    // input -> first hidden layer
                    m_Weights[i] = new float[iInputLayerSize][];
                    for (int j = 0; j < iInputLayerSize; j++)
                    {
                        m_Weights[i][j] = new float[iHiddenLayerSizes[0]];
                    }
                }
                else
                {
                    m_Weights[i] = new float[iHiddenLayerSizes[0]][];
                    for (int j = 0; j < iHiddenLayerSizes[0]; j++)
                    {
                        m_Weights[i][j] = new float[iNumOutputs];
                    }
                }
            }

            // Init recurrent weights
            m_RecurrentWeights = new float[iHiddenLayerSizes[0]][];
            for (int i = 0; i < m_RecurrentWeights.Length; i++)
            {
                m_RecurrentWeights[i] = new float[iHiddenLayerSizes[0]];
            }

            m_ResetWeights = new float[iHiddenLayerSizes[0]][];
            for (int i = 0; i < m_ResetWeights.Length; i++)
            {
                m_ResetWeights[i] = new float[iHiddenLayerSizes[0]];
            }

            m_UpdateWeights = new float[iHiddenLayerSizes[0]][];
            for (int i = 0; i < m_UpdateWeights.Length; i++)
            {
                m_UpdateWeights[i] = new float[iHiddenLayerSizes[0]];
            }

            // Insert Random Values Into Weights
            for (int i = 0; i < m_Weights.Length; i++)
            {
                for (int j = 0; j < m_Weights[i].Length; j++)
                {
                    for (int k = 0; k < m_Weights[i][j].Length; k++)
                    {
                        float fRand = UnityEngine.Random.Range(-1f, 1f);
                        m_Weights[i][j][k] = fRand;
                    }
                }
            }

            for (int i = 0; i < m_RecurrentWeights.Length; i++)
            {
                for (int j = 0; j < m_RecurrentWeights[i].Length; j++)
                {
                    float fRand = UnityEngine.Random.Range(-1f, 1f);
                    m_RecurrentWeights[i][j] = fRand;
                }
            }

            for (int i = 0; i < m_ResetWeights.Length; i++)
            {
                for (int j = 0; j < m_ResetWeights[i].Length; j++)
                {
                    float fRand = UnityEngine.Random.Range(-1f, 1f);
                    m_ResetWeights[i][j] = fRand;
                }
            }

            for (int i = 0; i < m_UpdateWeights.Length; i++)
            {
                for (int j = 0; j < m_UpdateWeights[i].Length; j++)
                {
                    float fRand = UnityEngine.Random.Range(-1f, 1f);
                    m_UpdateWeights[i][j] = fRand;
                }
            }
        }
        else if (sFilename != "")
        {
            //unpack
            List<NetworkStorageData> nNetList = NeuralNetworkUtils.ReadLinesIntoObjects(sFilename);
            if (!bPerserveGenerationAndSpecies)
            {
                m_iGeneration = nNetList[0].iGeneration;
                m_iSpecies = nNetList[0].iSpecies;
            }
            m_Weights = NeuralNetworkUtils.ConvertStorageWeights(nNetList[0].fWeights);
            m_RecurrentWeights = NeuralNetworkUtils.ConvertStorageRecurrentWeights(nNetList[0].fRecurWeights);
        }


        WriteData();

        //string[] lines = System.IO.File.ReadAllLines(@"C:\Programs\MeleeMasters\NetworkWeights.txt");

    }

    public void Forward()
    {
        iTrainingCount += 1;
        m_CurrentLayerValues = new float[iInputLayerSize];
        //pass inputs 
        Normalize(m_CarControl, ref m_CurrentLayerValues);
        //Normalize(m_Move, ref m_Inputs, enemyMv);

        //input layer to first hidden layer
        CalculateNextLayerValue(0, iInputLayerSize);
        CalculateRecurrentLayers();
        CalculateOutputLayer();
        UpdateFitness();
      
        //store the output weights, and current fitness in the storage array so we can later come back and train based on the result of this

    }
    private void CalculateOutputLayer()
    {
        m_NextLayerValues = new float[iNumOutputs];

        // Insert the next values, if we have exceeded the max previous timesteps, 
        // AND the appropriate number of frames has passed delete our old data.

        float[] newHiddenValues = new float[iHiddenLayerSizes[0]];
        m_CurrentLayerValues.CopyTo(newHiddenValues, 0);
        m_PreviousHiddenValues.Insert(0, newHiddenValues);
        m_iCurrentFrameDifference = 0;

        if (m_PreviousHiddenValues.Count > m_iMaxPreviousTimesteps)
        {
            m_PreviousHiddenValues.RemoveAt(m_PreviousHiddenValues.Count - 1);
        }


        for (int i = 0; i < iNumOutputs; i++)
        {
            float fSum = 0;
            for (int j = 0; j < iHiddenLayerSizes[0]; j++)
            {
                fSum += m_HiddenLayerValues[j] * m_Weights[m_Weights.Length - 1][j][i];
            }
            fSum += m_LayerBiases[m_Weights.Length - 1];
            m_NextLayerValues[i] = NeuralNetworkUtils.Sigmoid(fSum);
        }

        for (int i = 0; i < m_NextLayerValues.Length; i++)
        {
            m_NextLayerValues[i] = NeuralNetworkUtils.MinMax(m_NextLayerValues[i], 1, 0, 10, -10);
        }
        m_CurrentLayerValues = m_NextLayerValues;

        //two seperate softmaxes, one for the controller axes, and one for button pressed
        float[] fAxes = new float[8];
        float[] fButtons = new float[6];

        m_Outputs = m_CurrentLayerValues;
    }

    private void CalculateRecurrentLayersGRU()
    {
        m_previousHiddenLayerValues.Clear();
        m_NextLayerValues = new float[iHiddenLayerSizes[0]];
        float[] prevLayerValues = new float[iHiddenLayerSizes[0]];
        prevLayerValues = m_CurrentLayerValues;
        float[] lWorkingLayerValues = new float[iHiddenLayerSizes[0]];

        if (m_PreviousHiddenValues.Count > 0 || m_PreviousCellState.Count > 0)
        {
            lWorkingLayerValues = m_PreviousHiddenValues[0];

            //m_next = non-activated current values, activate them so we can do maths
            for (int k = 0; k < lWorkingLayerValues.Length; k++)
            {
                lWorkingLayerValues[k] = NeuralNetworkUtils.Sigmoid(lWorkingLayerValues[k]);
            }

            for (int j = 0; j < iHiddenLayerSizes[0]; j++)
            {
                float fSum = 0;
                for (int k = 0; k < iHiddenLayerSizes[0]; k++)
                {
                    fSum += lWorkingLayerValues[j] * m_RecurrentWeights[j][k];
                }
                fSum += m_LayerBiases[m_Weights.Length - 1];
                m_NextLayerValues[j] = fSum;
            }

            for (int i = 0; i < m_NextLayerValues.Length; i++)
            {
                m_NextLayerValues[i] += m_CurrentLayerValues[i];
            }

            //activate it? not sure, need to test out
            for (int k = 0; k < lWorkingLayerValues.Length; k++)
            {
                m_NextLayerValues[k] = NeuralNetworkUtils.Sigmoid(m_NextLayerValues[k]);
            }

            float[] fResetValues = new float[m_NextLayerValues.Length];
            float[] fUpdateValues = new float[m_NextLayerValues.Length];
            m_NextLayerValues.CopyTo(fResetValues, 0);
            m_NextLayerValues.CopyTo(fUpdateValues, 0);

            float[] hInput = new float[m_NextLayerValues.Length];
            hInput = MultiplyMatrix1D(m_NextLayerValues, m_PreviousHiddenValues[0]);

            for (int i = 0; i < hInput.Length; i++)
            {
                hInput[i] += m_CurrentLayerValues[i];
                hInput[i] = NeuralNetworkUtils.TanH(hInput[i]);
            }

            float[] fUpdateValues2 = new float[fUpdateValues.Length];
            fUpdateValues.CopyTo(fUpdateValues2, 0);

            float[] fOneArray = new float[fUpdateValues2.Length];
            for (int i = 0; i < fOneArray.Length; i++) { fOneArray[i] = 1; }

            fUpdateValues2 = ArrayMinus(fOneArray, fUpdateValues2);
            fUpdateValues2 = MultiplyMatrix1D(fUpdateValues2, m_PreviousHiddenValues[0]);

            fUpdateValues = MultiplyMatrix1D(fUpdateValues, hInput);
            m_NextLayerValues = ArrayAdd(fUpdateValues, fUpdateValues2);

            m_CurrentLayerValues = m_NextLayerValues;
        }
    }

    private void CalculateRecurrentLayersLSTM()
    {
        m_previousHiddenLayerValues.Clear();
        m_NextLayerValues = new float[iHiddenLayerSizes[0]];
        float[] prevLayerValues = new float[iHiddenLayerSizes[0]];
        prevLayerValues = m_CurrentLayerValues;
        float[] lWorkingLayerValues = new float[iHiddenLayerSizes[0]];

        if (m_PreviousHiddenValues.Count > 0 || m_PreviousCellState.Count > 0)
        {
            lWorkingLayerValues = m_PreviousHiddenValues[0];

            //m_next = non-activated current values, activate them so we can do maths
            for (int k = 0; k < lWorkingLayerValues.Length; k++)
            {
                lWorkingLayerValues[k] = NeuralNetworkUtils.Sigmoid(lWorkingLayerValues[k]);
            }

            for (int j = 0; j < iHiddenLayerSizes[0]; j++)
            {
                float fSum = 0;
                for (int k = 0; k < iHiddenLayerSizes[0]; k++)
                {
                    fSum += lWorkingLayerValues[j] * m_RecurrentWeights[j][k];
                }
                fSum += m_LayerBiases[m_Weights.Length - 1];
                m_NextLayerValues[j] = fSum;
            }

            for (int i = 0; i < m_NextLayerValues.Length; i++)
            {
                m_NextLayerValues[i] += m_CurrentLayerValues[i];
            }

            float[] fCellLayerValues = new float[m_NextLayerValues.Length];
            m_NextLayerValues.CopyTo(fCellLayerValues, 0);

            for (int i = 0; i < fCellLayerValues.Length; i++)
            {
                fCellLayerValues[i] = NeuralNetworkUtils.TanH(fCellLayerValues[i]);
            }

            //activate it? not sure, need to test out
            for (int k = 0; k < lWorkingLayerValues.Length; k++)
            {
                m_NextLayerValues[k] = NeuralNetworkUtils.Sigmoid(m_NextLayerValues[k]);
            }

            float[] fForgetLayerValues = new float[m_NextLayerValues.Length];
            float[] fInputLayerValues = new float[m_NextLayerValues.Length];
            m_NextLayerValues.CopyTo(fForgetLayerValues, 0);
            m_NextLayerValues.CopyTo(fInputLayerValues, 0);



            m_CurrentLayerValues = m_NextLayerValues;

        }
    }

    private void CalculateRecurrentLayers()
    {
        m_previousHiddenLayerValues.Clear();
        m_NextLayerValues = new float[iHiddenLayerSizes[0]];
        float[] prevLayerValues = new float[iHiddenLayerSizes[0]];
        prevLayerValues = m_CurrentLayerValues;
        float[] lWorkingLayerValues = new float[iHiddenLayerSizes[0]];

        if (m_PreviousHiddenValues.Count > 0)
        {
            lWorkingLayerValues = m_PreviousHiddenValues[0];

            //m_next = non-activated current values, activate them so we can do maths
            for (int k = 0; k < lWorkingLayerValues.Length; k++)
            {
                lWorkingLayerValues[k] = NeuralNetworkUtils.Sigmoid(lWorkingLayerValues[k]);
            }

            for (int j = 0; j < iHiddenLayerSizes[0]; j++)
            {
                float fSum = 0;
                for (int k = 0; k < iHiddenLayerSizes[0]; k++)
                {
                    fSum += lWorkingLayerValues[j] * m_RecurrentWeights[j][k];
                }
                fSum += m_LayerBiases[m_Weights.Length - 1];
                m_NextLayerValues[j] = fSum;
            }

            for (int i = 0; i < m_NextLayerValues.Length; i++)
            {
                m_NextLayerValues[i] += m_CurrentLayerValues[i];
            }

            //activate it? not sure, need to test out
            for (int k = 0; k < lWorkingLayerValues.Length; k++)
            {
                m_NextLayerValues[k] = NeuralNetworkUtils.Sigmoid(m_NextLayerValues[k]);
            }

            m_CurrentLayerValues = m_NextLayerValues;

        }

        for (int i = 1; i < m_PreviousHiddenValues.Count; i++)
        {
            m_NextLayerValues = new float[iHiddenLayerSizes[0]];
            lWorkingLayerValues = m_PreviousHiddenValues[i - 1];

            //first, compute the values for the new layer given the old values and recurrent weights
            for (int j = 0; j < iHiddenLayerSizes[0]; j++)
            {
                float fSum = 0;
                for (int k = 0; k < iHiddenLayerSizes[0]; k++)
                {
                    fSum += lWorkingLayerValues[j] * m_RecurrentWeights[j][k];
                }
                fSum += m_LayerBiases[m_Weights.Length - 1];
                m_NextLayerValues[j] = fSum;
            }

            //m_next = non-activated current values
            for (int k = 0; k < m_NextLayerValues.Length; k++)
            {
                m_NextLayerValues[k] = NeuralNetworkUtils.Sigmoid(m_NextLayerValues[k]);
            }

            for (int z = 0; z < m_NextLayerValues.Length; z++)
            {
                m_NextLayerValues[z] += m_PreviousHiddenValues[i][z];
            }

            //store un-activated layer values for future use
            float[] prevHiddenLayerVal = new float[prevLayerValues.Length];
            prevLayerValues.CopyTo(prevHiddenLayerVal, 0);
            m_previousHiddenLayerValues.Add(prevHiddenLayerVal);

            for (int k = 0; k < m_NextLayerValues.Length; k++)
            {
                m_NextLayerValues[k] = NeuralNetworkUtils.Sigmoid(m_NextLayerValues[k]);
            }

            for (int j = 0; j < iHiddenLayerSizes[0]; j++)
            {
                float fSum = 0;
                for (int k = 0; k < iHiddenLayerSizes[0]; k++)
                {
                    fSum += prevLayerValues[j] * m_RecurrentWeights[j][k];
                }
                fSum += m_LayerBiases[m_Weights.Length - 1];
                m_NextLayerValues[j] = fSum;
            }

            prevLayerValues = m_NextLayerValues;
        }
    }

    private void CalculateNextLayerValue(int iLayer, int iCurrentLayerSize)
    {
        for (int iNextIndex = 0; iNextIndex < iHiddenLayerSizes[iLayer]; iNextIndex++)
        {
            float fSum = 0;
            for (int i = 0; i < iCurrentLayerSize; i++)
            {
                fSum += m_CurrentLayerValues[i] * m_Weights[iLayer][i][iNextIndex];
            }
            //fSum += m_LayerBiases[iLayer];
            m_HiddenLayerValues[iNextIndex] = NeuralNetworkUtils.Sigmoid(fSum);
        }
        m_CurrentLayerValues = m_HiddenLayerValues;
    }

    private void Normalize(CarControl cc, ref float[] inputWeights)
    {

        inputWeights[iPositionXIdx] = 0;
        inputWeights[iPositionZIdx] = 0;
        inputWeights[iVelocityXIdx] = cc.v3MoveDirection.x * fVelocityMultiplier;
        inputWeights[iVelocityZIdx] = cc.v3MoveDirection.z * fVelocityMultiplier;

        VisionHit vh = cc.GetQuadrantObject(1);
        if (vh != null)
        {
            inputWeights[iQuadrant1Distance] = (cc.transform.position - vh.v3RayCastHitPosition).magnitude;
            if (vh.goOject.GetComponent<TargetTrigger>())
            {
                inputWeights[iQuadrant1TargetOrObsIdx] = 1;
            }
            else
            {
                inputWeights[iQuadrant1TargetOrObsIdx] = -1;
            }
        }
        else
        {
            inputWeights[iQuadrant1Distance] = ObjectsTrigger.fCheckDistance; //max
            inputWeights[iQuadrant1TargetOrObsIdx] = 0;
        }

        vh = cc.GetQuadrantObject(2);
        if (vh != null)
        {
            inputWeights[iQuadrant2Distance] = (cc.transform.position - vh.v3RayCastHitPosition).magnitude;
            if (vh.goOject.GetComponent<TargetTrigger>())
            {
                inputWeights[iQuadrant2TargetOrObsIdx] = 1;
            }
            else
            {
                inputWeights[iQuadrant2TargetOrObsIdx] = -1;
            }
        }
        else
        {
            inputWeights[iQuadrant2Distance] = ObjectsTrigger.fCheckDistance; //max
            inputWeights[iQuadrant2TargetOrObsIdx] = 0;
        }

        vh = cc.GetQuadrantObject(3);
        if (vh != null)
        {
            inputWeights[iQuadrant3Distance] = (cc.transform.position - vh.v3RayCastHitPosition).magnitude;
            if (vh.goOject.GetComponent<TargetTrigger>())
            {
                inputWeights[iQuadrant3TargetOrObsIdx] = 1;
            }
            else
            {
                inputWeights[iQuadrant3TargetOrObsIdx] = -1;
            }
        }
        else
        {
            inputWeights[iQuadrant3Distance] = ObjectsTrigger.fCheckDistance; //max
            inputWeights[iQuadrant3TargetOrObsIdx] = 0;
        }

        vh = cc.GetQuadrantObject(4);
        if (vh != null)
        {
            inputWeights[iQuadrant4Distance] = (cc.transform.position - vh.v3RayCastHitPosition).magnitude;
            if (vh.goOject.GetComponent<TargetTrigger>())
            {
                inputWeights[iQuadrant4TargetOrObsIdx] = 1;
            }
            else
            {
                inputWeights[iQuadrant4TargetOrObsIdx] = -1;
            }
        }
        else
        {
            inputWeights[iQuadrant4Distance] = ObjectsTrigger.fCheckDistance; //max
            inputWeights[iQuadrant4TargetOrObsIdx] = 0;
        }

        vh = cc.GetQuadrantObject(5);
        if (vh != null)
        {
            inputWeights[iQuadrant5Distance] = (cc.transform.position - vh.v3RayCastHitPosition).magnitude;
            if (vh.goOject.GetComponent<TargetTrigger>())
            {
                inputWeights[iQuadrant5TargetOrObsIdx] = 1;
            }
            else
            {
                inputWeights[iQuadrant5TargetOrObsIdx] = -1;
            }
        }
        else
        {
            inputWeights[iQuadrant5Distance] = ObjectsTrigger.fCheckDistance; //max
            inputWeights[iQuadrant5TargetOrObsIdx] = 0;
        }

        vh = cc.GetQuadrantObject(6);
        if (vh != null)
        {
            inputWeights[iQuadrant6Distance] = (cc.transform.position - vh.v3RayCastHitPosition).magnitude;
            if (vh.goOject.GetComponent<TargetTrigger>())
            {
                inputWeights[iQuadrant6TargetOrObsIdx] = 1;
            }
            else
            {
                inputWeights[iQuadrant6TargetOrObsIdx] = -1;
            }
        }
        else
        {
            inputWeights[iQuadrant6Distance] = ObjectsTrigger.fCheckDistance; //max
            inputWeights[iQuadrant6TargetOrObsIdx] = 0;
        }

        vh = cc.GetQuadrantObject(7);
        if (vh != null)
        {
            inputWeights[iQuadrant7Distance] = (cc.transform.position - vh.v3RayCastHitPosition).magnitude;
            if (vh.goOject.GetComponent<TargetTrigger>())
            {
                inputWeights[iQuadrant7TargetOrObsIdx] = 1;
            }
            else
            {
                inputWeights[iQuadrant7TargetOrObsIdx] = -1;
            }
        }
        else
        {
            inputWeights[iQuadrant7Distance] = ObjectsTrigger.fCheckDistance; //max
            inputWeights[iQuadrant7TargetOrObsIdx] = 0;
        }

        vh = cc.GetQuadrantObject(8);
        if (vh != null)
        {
            inputWeights[iQuadrant8Distance] = (cc.transform.position - vh.v3RayCastHitPosition).magnitude;
            if (vh.goOject.GetComponent<TargetTrigger>())
            {
                inputWeights[iQuadrant8TargetOrObsIdx] = 1;
            }
            else
            {
                inputWeights[iQuadrant8TargetOrObsIdx] = -1;
            }
        }
        else
        {
            inputWeights[iQuadrant8Distance] = ObjectsTrigger.fCheckDistance; //max
            inputWeights[iQuadrant8TargetOrObsIdx] = 0;
        }

        m_Inputs = inputWeights;
    }

    public void SetInputs(ref List<string> sButtons)
    {
        sButtons.Clear();
        if (m_CurrentLayerValues[0] > iMinButtonPressThreshold)
        {
            sButtons.Add("w");
        }

        if (m_CurrentLayerValues[1] > iMinButtonPressThreshold)
        {
            sButtons.Add("a");
        }

        if (m_CurrentLayerValues[2] > iMinButtonPressThreshold)
        {
            sButtons.Add("d");
        }

        if (m_CurrentLayerValues[3] > iMinButtonPressThreshold)
        {
            sButtons.Add("s");
        }
    }

    public float Sigmoid(float f)
    {
        return 1 / (1 + Mathf.Exp(-f));
    }

    public float[] Sigmoid(float[] f)
    {
        float[] fRet = new float[f.Length];
        for (int i = 0; i < f.Length; i++)
        {
            fRet[i] = Sigmoid(f[i]);
        }
        return fRet;
    }

    public float SigmoidPrime(float f)
    {
        return Mathf.Exp(-f) / ((1 + Mathf.Exp(-f)) * 2 * 2);
    }

    public float[] SigmoidPrime(float[] f)
    {
        float[] fRet = new float[f.Length];
        for (int i = 0; i < f.Length; i++)
        {
            fRet[i] = SigmoidPrime(f[i]);
        }
        return fRet;
    }

    public float[] Cost()
    {
        float[] fPrediction = new float[m_Outputs.Length];
        //Forward();
        fPrediction = m_Outputs;
        float[] J = new float[3];/*0.5f * sum(ArrayMinus(fActual, fPrediction)) * 2 * 2);*/
        return J;
    }

    public float MatrixDot(float[] fInput, float[] fInput2)
    {
        float fSum = 0;
        for (int i = 0; i < fInput.Length; i++)
        {
            fSum += fInput[i] * fInput2[i];
        }
        return fSum;
    }

    public void CostFunctionPrime(int[] fModificationArray)
    {
        //the fmodification array is an array for either 0's or ones, 
        //representing weather or not to train a respective output node upwards or downwards
        //calculate 
        float[] fError = new float[m_Outputs.Length];
        float[] fPrediction = new float[m_Outputs.Length];
        m_HiddenLayerDeltas = new List<float[][]>();
        fPrediction = m_Outputs;
        for (int i = 0; i < fModificationArray.Length; i++)
        {
            fError[i] = GetAdjustedVal(i, fPrediction[i], fModificationArray[i]);
        }

        float[][] delta3 = MatrixSpread(
            MultiplyMatrix1D(
                ArrayMultiply(ArrayMinus(fError, fPrediction), -1),
                SigmoidPrime(m_Outputs)));

        float[][] hiddenLayerActivatedTranspose = TransposeMatrix(
            MatrixSpread(m_previousHiddenLayerValues[m_previousHiddenLayerValues.Count - 1]));
        float[][] dJdWl = MultiplyMatrix2D(hiddenLayerActivatedTranspose, delta3);
        m_HiddenLayerDeltas.Add(dJdWl);

        // this is what we will roll through the hidden layers with
        float[][] prevDelta = new float[m_CurrentLayerValues.Length][];

        for (int p = 0; p < delta3.Length; p++)
            prevDelta = delta3;

        float[][] delta = MatrixSpread(
                MultiplyMatrix2D1D(
                    TransposeMatrix(MultiplyMatrix2D(
                        m_Weights[1], TransposeMatrix(prevDelta))),
                    SigmoidPrime(m_previousHiddenLayerValues[m_previousHiddenLayerValues.Count - 1])));

        float[][] djdWH = MultiplyMatrix2D(
            TransposeMatrix(
                MatrixSpread(
                    Sigmoid(m_previousHiddenLayerValues[m_previousHiddenLayerValues.Count - 1]))), delta);
        m_HiddenLayerDeltas.Add(djdWH);
        prevDelta = delta;

        float[][] inputDelta = MatrixSpread(
            MultiplyMatrix2D1D(
                MultiplyMatrix2D(
                    TransposeMatrix(m_Weights[0]), prevDelta), SigmoidPrime(m_previousHiddenLayerValues[0])));
        float[][] djdWI = MultiplyMatrix2D(TransposeMatrix(MatrixSpread(m_Inputs)), inputDelta);
        m_HiddenLayerDeltas.Add(djdWI);

    }

    public void CostFunction(float[] fPrediction)
    {
        //basically just calls forward and gets output based on the old prediction
    }

    public void Train(int[] fModificationArray, float fLearningRate = 1)
    {
        //populates the list of deltas
        CostFunctionPrime(fModificationArray);

        //now apply the deltas to the network
        //ApplyDeltas(fLearningRate);

    }

    public void Train(float[] fFloatArray, float fOldFitness, float fNewFitness)
    {
        int[] fModificationArray = new int[fFloatArray.Length];

        for (int i = 0; i < fModificationArray.Length; i++)
        {
            fModificationArray[i] = GetErrorValue(i, fFloatArray[i], fOldFitness < fNewFitness);
        }

        CostFunctionPrime(fModificationArray);

        ApplyDeltas(1);
    }

    public void ApplyDeltas(float fLearningRate)
    {
        // m_Weights[0] = ArrayAdd(m_Weights[0], m_Deltas[0] * fLearningRate);
        m_Weights[1] = ArrayMinus(m_Weights[1], ArrayMultiply(m_HiddenLayerDeltas[0], fLearningRate));
        m_RecurrentWeights = ArrayMinus(m_RecurrentWeights, ArrayMultiply(m_HiddenLayerDeltas[1], -fLearningRate));
        m_Weights[0] = ArrayMinus(m_Weights[0], ArrayMultiply(m_HiddenLayerDeltas[2], fLearningRate));
    }

    public float[] ActivateLayer(float[] fOutputs)
    {
        float[] fRet = new float[fOutputs.Length];
        for (int i = 0; i < fRet.Length; i++)
        {
            fRet[i] = Sigmoid(fOutputs[i]);
        }
        return fRet;
    }

    public float[][] MultiplyMatrix2D(float[][] a, float[][] b)
    {
        float[][] c = new float[a.Length][];
        if (a[0].Length == b.Length)
        {
            for (int i = 0; i < c.Length; i++)
            {
                c[i] = new float[b[0].Length];
                for (int j = 0; j < c[i].Length; j++)
                {
                    c[i][j] = 0;
                    for (int k = 0; k < a[0].Length; k++)
                        c[i][j] = c[i][j] + a[i][k] * b[k][j];
                }
            }
        }
        else
        {
            if (a.Length == b[0].Length)
            {
                c = new float[a.Length][];
                for (int i = 0; i < c.Length; i++)
                {
                    c[i] = new float[b[0].Length];
                    for (int j = 0; j < c[i].Length; j++)
                    {
                        c[i][j] = 0;
                        for (int k = 0; k < b.Length; k++)
                            c[i][j] = c[i][j] + a[i][k] * b[k][j];
                    }
                }
            }
            else
            {
                Console.WriteLine("\n Number of columns in Matrix1 is not equal to Number of rows in Matrix2.");
                Console.WriteLine("\n Therefore Multiplication of Matrix1 with Matrix2 is not possible");
            }
        }
        return c;
    }

    public float[][] TransposeMatrix(float[][] fInput)
    {
        float[][] fRet = new float[fInput[0].Length][];
        for (int i = 0; i < fInput[0].Length; i++)
        {
            fRet[i] = new float[fInput.Length];
            for (int j = 0; j < fInput.Length; j++)
            {
                fRet[i][j] = fInput[j][i];
            }
        }
        return fRet;
    }

    public float[] MultiplyMatrix2D1D(float[][] a, float[] b)
    {
        float[] c = new float[b.Length];
        if (a[0].Length == b.Length)
        {
            for (int k = 0; k < a[0].Length; k++)
                c[k] = c[k] + a[0][k] * b[k];
        }
        else
        {
            //Console.WriteLine(“\n Number of columns in Matrix1 is not equal to Number of rows in Matrix2.”);
            //Console.WriteLine(“\n Therefore Multiplication of Matrix1 with Matrix2 is not possible”);
        }
        return c;
    }

    public float[] MultiplyMatrix1D(float[] a, float[] b)
    {
        float[] c = new float[a.Length];
        if (a.Length == b.Length)
        {
            for (int i = 0; i < c.Length; i++)
            {
                c[i] = a[i] * b[i];
            }
        }
        else
        {
            //bad call
            return null;
        }
        return c;
    }

    public float[] ArrayMultiply(float[] lhs, float fVal)
    {
        float[] fRet = new float[lhs.Length];
        for (int i = 0; i < lhs.Length; i++)
        {
            fRet[i] = lhs[i] * fVal;
        }
        return fRet;
    }

    public float[][] ArrayMultiply(float[][] lhs, float fVal)
    {
        float[][] fRet = new float[lhs.Length][];
        for (int i = 0; i < lhs.Length; i++)
        {
            fRet[i] = new float[lhs[i].Length];
            for (int j = 0; j < lhs[i].Length; j++)
            {
                fRet[i][j] = lhs[i][j] * fVal;
            }
        }
        return fRet;
    }

    public float[][] ArrayAdd(float[][] lhs, float fVal)
    {
        float[][] fRet = new float[lhs.Length][];
        for (int i = 0; i < lhs.Length; i++)
        {
            fRet[i] = new float[lhs[i].Length];
            for (int j = 0; j < lhs[i].Length; j++)
            {
                fRet[i][j] = lhs[i][j] + fVal;
            }
        }
        return fRet;
    }

    public float[] ArrayMinus(float[] lhs, float[] rhs)
    {
        float[] fRet = new float[lhs.Length];
        for (int i = 0; i < lhs.Length; i++)
        {
            fRet[i] = lhs[i] - rhs[i];
        }
        return fRet;
    }

    public float[] ArrayAdd(float[] lhs, float[] rhs)
    {
        float[] fRet = new float[lhs.Length];
        for (int i = 0; i < lhs.Length; i++)
        {
            fRet[i] = lhs[i] + rhs[i];
        }
        return fRet;
    }

    public float[][] ArrayMinus(float[][] lhs, float[][] rhs)
    {
        float[][] fRet = new float[lhs.Length][];
        for (int i = 0; i < lhs.Length; i++)
        {
            fRet[i] = new float[lhs[i].Length];
            for (int j = 0; j < lhs[i].Length; j++)
            {
                fRet[i][j] = lhs[i][j] - rhs[i][j];
            }
        }
        return fRet;
    }

    public float sum(float[] fIn)
    {
        float fRet = 0;
        for (int i = 0; i < fIn.Length; i++)
        {
            fRet += fIn[i];
        }
        return fRet;
    }

    public float[][] MatrixSpread(float[] fInput)
    {
        float[][] fRet = new float[1][];
        fRet[0] = new float[fInput.Length];
        for (int i = 0; i < fInput.Length; i++)
        {
            fRet[0][i] = fInput[i];
        }

        return fRet;
    }

    public void UnrollNetwork()
    {
        float[] Djdw = new float[m_CurrentHiddenLayerValues.Length];

    }

    public void BackProp()
    {
        float[] prediction = m_Outputs;
        float[] error = new float[prediction.Length];

        //the actions we took (((time step))) ago benefited us by delta amount or hurt us by delta amount
        //fDeltaFitness = GetFitness() - m_previousFitnessTimestep;

        for (int i = 0; i < prediction.Length; i++)
        {
            if (UnityEngine.Random.Range(0f, 1f) < 0.5f)
            {
                /*
                if (fDeltaFitness > 0)
                {
                    error[i] = prediction[i] * fCorrectionIncreasePercentage;
                }
                else
                {
                    error[i] = prediction[i] * -fCorrectionIncreasePercentage;
                }
                */
            }
        }


    }

    private float GetAdjustedVal(int iIndex, float iInput, int bMaximize)
    {
        float fRet = 0;

        switch (iIndex)
        {
            case 0:
            case 1:
                if (bMaximize == 0)
                {

                    //we can't maximize something done well, because done well is subjective.
                    //but if we determine this is bad, we can zero it out as something we DON'T want. 
                    fRet = 0;
                }
                else
                {
                    fRet = iInput;
                }
                break;
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                if (iInput > iMinButtonPressThreshold)
                {
                    if (bMaximize == 1)
                        fRet = 10;
                    else
                        fRet = 0;
                }
                break;
            default:
                break;
        }

        return fRet;
    }

    private int GetErrorValue(int iIndex, float fInput, bool bMax)
    {
        int iRet = 0;

        switch (iIndex)
        {
            case 0:
            case 1:
                if (!bMax)
                {

                    //we can't maximize something done well, because done well is subjective.
                    //but if we determine this is bad, we can zero it out as something we DON'T want. 
                    iRet = 0;
                }
                else
                {
                    iRet = 1;
                }
                break;
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                if (fInput > iMinButtonPressThreshold)
                {
                    if (bMax)
                        iRet = 1;
                    else
                        iRet = 0;
                }
                else
                {
                    if (bMax)
                        iRet = 0;
                    else
                        iRet = 1;
                }
                break;
            default:
                break;
        }

        return iRet;
    }

    private void ComputeGradients(int[] iTrainingDirections)
    {
        //CostFunctionPrime(iTrainingDirections);

    }

    //Progress is true if 
    private void Train(bool bProgress)
    {
        int[] iTrainingDirections = new int[m_Outputs.Length];

        for (int i = 0; i < iTrainingDirections.Length; i++)
        {
            iTrainingDirections[i] = GetErrorValue(i, m_Outputs[i], bProgress);
        }

        ComputeGradients(iTrainingDirections);

        //ApplyDeltas(m_fLearningRate);
    }

    public void ApplyReinforcementSet(ReInforcementSet r)
    {

    }
}
