﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct ControllerInput
{
    public Vector2 v2ControllerAxis;
    public Vector2 v2RightStickAxis;
    public float fDodgeAxis;
    public string[] m_sButtons;

    public ControllerInput(InputInterface i)
    {
        v2ControllerAxis = i.v2ControllerAxis;
        v2RightStickAxis = i.v2SAxis;
        fDodgeAxis = i.fDodgeAxis;
        m_sButtons = i.sNetButtons;
    }

    public ControllerInput(Vector2 v2controller, Vector2 v2RightStick, float fDodge, List<string> sButtons)
    {
        v2ControllerAxis = v2controller;
        v2RightStickAxis = v2RightStick;
        fDodgeAxis = fDodge;
        m_sButtons = new string[sButtons.Count];
        for (int j = 0; j < sButtons.Count; j++)
        {
            m_sButtons[j] = sButtons[j];
        }
    }
}

public class InputInterface : MonoBehaviour
{
    public int playerID = 0;

    private static string sResourceDir = "Config\\";


    private List<string> m_sButtons = new List<string>();
    private List<string> m_sButtonWasDown;
    private Dictionary<string, int> m_Buttons = new Dictionary<string, int>();
    private Dictionary<string, int> m_ButtonsPrev = new Dictionary<string, int>();
    private Vector2 m_fControllerAxis;
    private Vector2 m_fControllerSAxis;


    private bool bSimulationControlled;
    public bool bPlayerControlled;
    private bool bReplayControlled = false;

    private bool bPlayerControlInit;
    private bool bReplayControlledInit;

    private bool bLastInputWasKeyboard = false;

    public AIControl m_AIController;

    public bool bIsKeyboardControlled;

    public Vector2 v2ControllerAxis;
    public Vector2 v2SAxis;
    public float fDodgeAxis;


    private string[] m_sButtonList = { "w", "a", "d", "s" };
    private string[] sbuttonlist = { "w", "a", "d", "s" };

    public float fXDeadZoneRange = 3f;
    public float fYDeadZoneRange = 3f;
    public float fXSDeadZoneRange = 3f;
    public float fYSDeadZoneRange = 3f;

    public bool bTapJump = false;
    public float fTapJumpSensitivity = 4;
    public float fWalkSensitivity = 4.5f;
    public float fStickMaxSensitivity = 9.8f;

    public float fDashSensitivity = 2.5f;

    public float fSpotDodgeSensitivity = 5.0f;
    public float fRollSensitivity = 1.75f;

    public float fPlatformDropSensitivity = 4.0f;

    public float fVerticalSensitivty = 6.0f;

    public float fRightStickActivationSens = 6.0f;


    //[SyncVar]
    //private SyncListString sNetworkButtonsHit = new SyncListString();
    public string[] sNetButtons = new string[9];


    public int m_iNetInstantiationId = -1;

    bool isPaused = false;

    void OnApplicationFocus(bool hasFocus)
    {
        isPaused = !hasFocus;
    }

    void OnApplicationPause(bool pauseStatus)
    {
        isPaused = pauseStatus;
    }


    bool FindInList(List<string> stringList, string sName)
    {
        for (int i = 0; i < stringList.Count; i++)
        {
            if (stringList[i] == sName)
            {
                return true;
            }
        }
        return false;
    }

   
    public bool GetButton(string sButton)
    {
        bool bRet = false;
        if (bPlayerControlled)
        {
            if (!isPaused)
            {
                if (Input.GetButton(sButton))
                {
                    bRet = true;
                }
            }
        }
        else
        {
            //if (FindInList(m_sButtons,sButton))
            if (m_sButtons.Contains(sButton))
            {
                if (m_Buttons[sButton] == 1)
                {
                    bRet = true;
                }
            }
        }
        return bRet;
    }

    public bool GetButtonDown(string sButton)
    {
        bool bRet = false;
        if (bPlayerControlled)
        {
            if (!isPaused)
            {
                if (Input.GetButtonDown(sButton))
                {
                    bRet = true;
                }
            }
        }
        else
        {
            //if (FindInList(m_sButtons,sButton))
            if (m_Buttons[sButton] == 1 && m_ButtonsPrev[sButton] == 0)
            {
                bRet = true;
            }
        }
        return bRet;
    }

    public bool GetButtonUp(string sButton)
    {
        bool bRet = false;
        if (bPlayerControlled)
        {
            if (!isPaused)
            {
                if (Input.GetButtonUp(sButton))
                {
                    bRet = true;
                }
            }
        }
        else
        {
            //if (FindInList(m_sButtons,sButton))
            if (m_sButtons.Contains(sButton))
            {
                if (m_Buttons[sButton] == 0 && m_ButtonsPrev[sButton] == 1)
                {
                    bRet = true;
                }
            }
        }
        return bRet;
    }

    public float GetAxis(string sAxis)
    {

        float fRet = 0;

        if (sAxis == "Horizontal")
        {
            if (bPlayerControlled)
            {
                if (!isPaused)
                    fRet = Input.GetAxis("Horizontal");

                if (Input.GetButton("SoftLimitControlStick"))
                    fRet *= 0.6f;
                else if (Input.GetButton("HardLimitControlStick"))
                    fRet *= 0.3f;

            }
            else
            {
                fRet = m_fControllerAxis.x;
            }
        }
        else if (sAxis == "Vertical")
        {
            if (bPlayerControlled)
            {
                if (!isPaused)
                    fRet = Input.GetAxis("Vertical");


                if (Input.GetButton("SoftLimitControlStick"))
                    fRet *= 0.6f;
                else if (Input.GetButton("HardLimitControlStick"))
                    fRet *= 0.3f;
            }
            else
            {
                fRet = m_fControllerAxis.y;
            }
        }
        else if (sAxis == "SX")
        {
            if (bPlayerControlled)
            {
                if (!isPaused)
                    fRet = Input.GetAxis("SX");
            }
            else
            {
                fRet = m_fControllerSAxis.x;
            }
        }
        else if (sAxis == "SY")
        {
            if (bPlayerControlled)
            {
                if (!isPaused)
                    fRet = Input.GetAxis("SY");
            }
            else
            {
                fRet = m_fControllerSAxis.y;
            }
        }
        else if (sAxis == "Dodge")
        {
            if (bPlayerControlled)
            {
                if (!isPaused)
                {
                    fRet = Input.GetAxis("Dodge");
                    float fRet2 = Input.GetAxis("DodgeR");

                    if (fRet2 > fRet)
                        fRet = fRet2;
                }
            }
            else
            {
                fRet = m_Buttons["Dodge"] == 1 ? 1 : 0;
            }
        }
        else if (sAxis == "DodgeR")
        {
            if (bPlayerControlled)
            {

            }
            else
            {
                fRet = m_Buttons["DodgeR"] == 1 ? 1 : 0;
            }
        }

        return fRet;
    }

    public void SetControllerAxis(Vector2 v2Axis)
    {
        m_fControllerAxis.x = v2Axis.x;
        m_fControllerAxis.y = v2Axis.y;
    }

    public void SetControllerSAxis(Vector2 v2Axis)
    {
        m_fControllerSAxis.x = v2Axis.x;
        m_fControllerSAxis.y = v2Axis.y;
    }

    public void SetAxes(List<float> fFloatList)
    {
        m_fControllerAxis.x = fFloatList[0];
        m_fControllerAxis.y = fFloatList[1];
        m_fControllerSAxis.x = fFloatList[2];
        m_fControllerSAxis.y = fFloatList[3];
    }

    public void SetButtons(List<string> sInputButtonList)
    {
        

        m_ButtonsPrev = new Dictionary<string, int>(m_Buttons);
        for (int i = 0; i < m_sButtonList.Length; i++)
        {
            if (sInputButtonList.Contains(m_sButtonList[i]))
            {
                m_Buttons[m_sButtonList[i]] = 1;
            }
            else
            {
                m_Buttons[m_sButtonList[i]] = 0;
            }
        }
    }

    public void SetReplayControlled()
    {
        bPlayerControlled = false;
        bReplayControlled = true;
    }

    public void SetAIControlled()
    {
        bPlayerControlled = false;
    }

    public bool IsAIControlled()
    {
        return !bPlayerControlled;
    }

    public void ResetControl()
    {
        //restore initial state
        bPlayerControlled = bPlayerControlInit;
        bReplayControlled = bReplayControlledInit;
    }

    // Use this for initialization
    void Start()
    {
        if (!bReplayControlled)
        {
            if (!m_AIController)
            {
                m_AIController = GetComponent<AIControl>();
                if (!m_AIController)
                {
                    bPlayerControlled = true;
                }
            }
            else
            {
                bPlayerControlled = false;
            }
        }

        m_Buttons = new Dictionary<string, int>();
        m_ButtonsPrev = new Dictionary<string, int>();
        for (int i = 0; i < m_sButtonList.Length; i++)
        {
            m_Buttons.Add(m_sButtonList[i], 0);
            m_ButtonsPrev.Add(m_sButtonList[i], 0);
        }
        bPlayerControlInit = bPlayerControlled;
        bReplayControlledInit = bReplayControlled;
        for (int i = 0; i < m_sButtonList.Length; i++)
        {
            m_sButtons.Add(m_sButtonList[i]);
        }
    }

    void Update()
    {
        if (!bPlayerControlled)
        {
            for (int j = 0; j < m_sButtonList.Length; j++)
            {
                m_ButtonsPrev[m_sButtonList[j]] = m_Buttons[m_sButtonList[j]];
            }

            for (int i = 0; i < m_sButtonList.Length; i++)
            {
                m_Buttons[m_sButtonList[i]] = 0;
            }

        }
        else
        {

        }
    }

    /*
    [Command]
    public void CmdSetPlayerButtonsAndAxes(Vector2 v2ControllerAxis, Vector2 v2SAxis, float fDodgeAxis, string[] sButtonsHit)
    {
        m_fControllerAxis = v2ControllerAxis;
        m_fControllerSAxis = v2SAxis;
        m_sButtons.Clear();
        
        for (int j = 0; j < sButtonsHit.Length; j++)
        {
            if (sButtonsHit[j] != "")
                m_sButtons.Add(sButtonsHit[j]);
        }

        if (fDodgeAxis > 4)
        {
            m_Buttons["Dodge"] = 1;
        }
        else
        {
            m_Buttons["Dodge"] = 0;
        }
    }

    
    [Command]
    public void CmdSetAuth(NetworkInstanceId objectId, NetworkIdentity player)
    {
        var iObject = NetworkServer.FindLocalObject(objectId);
        var networkIdentity = iObject.GetComponent<NetworkIdentity>();
        var otherOwner = networkIdentity.clientAuthorityOwner;

        if (otherOwner == player.connectionToClient)
        {
            return;
        }
        else
        {
            if (otherOwner != null)
            {
                networkIdentity.RemoveClientAuthority(otherOwner);
            }
            networkIdentity.AssignClientAuthority(player.connectionToClient);
        }
    }
    */

    public void ClearButtons()
    {
        for (int i = 0; i < m_sButtonList.Length; i++)
        {
            m_Buttons[m_sButtonList[i]] = 0;
        }
    }

    public void SetPlayerButtonsAndAxes(ControllerInput ci)
    {
        m_fControllerAxis = ci.v2ControllerAxis;
        m_fControllerSAxis = ci.v2RightStickAxis;
        m_sButtons.Clear();

        for (int j = 0; j < ci.m_sButtons.Length; j++)
        {
            if (ci.m_sButtons[j] != "")
                m_sButtons.Add(ci.m_sButtons[j]);
        }

        if (ci.fDodgeAxis > 4)
        {
            m_Buttons["Dodge"] = 1;
        }
        else
        {
            m_Buttons["Dodge"] = 0;
        }
    }

    public void SetPlayerButtonsAndAxes(Vector2 v2ControllerAxis, Vector2 v2SAxis, float fDodgeAxis, string[] sButtonsHit)
    {
        m_fControllerAxis = v2ControllerAxis;
        m_fControllerSAxis = v2SAxis;
        m_sButtons.Clear();

        for (int j = 0; j < sButtonsHit.Length; j++)
        {
            if (sButtonsHit[j] != "")
                m_sButtons.Add(sButtonsHit[j]);
        }

        if (fDodgeAxis > 4)
        {
            m_Buttons["Dodge"] = 1;
        }
        else
        {
            m_Buttons["Dodge"] = 0;
        }
    }

    public void GetInput(ref ControllerInput ci)
    {

        ci.v2ControllerAxis.x = Input.GetAxis("Horizontal");
        ci.v2ControllerAxis.y = Input.GetAxis("Vertical");
        ci.v2RightStickAxis.x = Input.GetAxis("SX");
        ci.v2RightStickAxis.y = Input.GetAxis("SY");

        ci.fDodgeAxis = Input.GetAxis("Dodge");
        float fRet2 = Input.GetAxis("DodgeR");

        if (fRet2 > fDodgeAxis)
            ci.fDodgeAxis = fRet2;

        m_sButtons.Clear();
        for (int i = 0; i < sbuttonlist.Length; i++)
        {
            if (Input.GetButton(sbuttonlist[i]))
            {
                m_sButtons.Add(sbuttonlist[i]);
            }
        }

        ci.m_sButtons = new string[m_sButtons.Count];
        for (int k = 0; k < ci.m_sButtons.Length; k++)
        {
            ci.m_sButtons[k] = m_sButtons[k];
        }

    }

    public void SetSimulationStatus(bool bStatus)
    {
        bPlayerControlled = bStatus;
    }

    public void SetInput(ControllerInput CI)
    {
        m_fControllerAxis = CI.v2ControllerAxis;
        m_fControllerSAxis = CI.v2RightStickAxis;
        fDodgeAxis = CI.fDodgeAxis;

        //set the previous from the last frame
        for (int j = 0; j < m_sButtonList.Length; j++)
        {
            m_ButtonsPrev[m_sButtonList[j]] = m_Buttons[m_sButtonList[j]];
        }

        //clear current buttons list
        for (int i = 0; i < m_sButtonList.Length; i++)
        {
            m_Buttons[m_sButtonList[i]] = 0;
        }

        //get new buttons from input
        for (int k = 0; k < CI.m_sButtons.Length; k++)
        {
            m_Buttons[CI.m_sButtons[k]] = 1;
        }
    }

    /*
    [ClientRpc]
    public void RpcSetPlayerButtonsAndAxes(Vector2 v2ControllerAxis, Vector2 v2SAxis, float fDodgeAxis, string[] sButtonsHit)
    {
        m_fControllerAxis = v2ControllerAxis;
        m_fControllerSAxis = v2SAxis;
        m_sButtons.Clear();

        for (int j = 0; j < sButtonsHit.Length; j++)
        {
            if (sButtonsHit[j] != "")
                m_sButtons.Add(sButtonsHit[j]);
        }

        if (fDodgeAxis > 4)
        {
            m_Buttons["Dodge"] = 1;
        }
        else
        {
            m_Buttons["Dodge"] = 0;
        }
    }

    void OnNetworkInstantiate(NetworkMessageInfo info)
    {
        Debug.Log("New object instantiated by " + info.sender);
    }
    */

}