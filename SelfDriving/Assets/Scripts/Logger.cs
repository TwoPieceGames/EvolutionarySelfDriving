﻿using UnityEngine;
using System.Collections;

public class Logger
{
    public static string m_sLoggingDir = "C:\\Programs\\SelfDriving\\Logging\\";
    private System.IO.StreamWriter m_fhFile;
    private string m_sFilename;
    private string m_sFileDirectory;

    public Logger(string sFileDir, string sFilename, bool bClearExisting = false)
    {
        m_sFilename = sFilename;
        m_sFileDirectory = sFileDir;
    }

    public bool Write(string sInputString)
    {
        //return false if unable to write
        bool bRet = true;
        m_fhFile = new System.IO.StreamWriter(m_sFileDirectory + m_sFilename, true);
        m_fhFile.Write(sInputString);
        m_fhFile.Close();

        return bRet;
    }

    public bool WriteLine(string sInputString)
    {
        bool bRet = true;
        m_fhFile = new System.IO.StreamWriter(m_sFilename, true);
        m_fhFile.Write(sInputString + "\n");
        m_fhFile.Close();

        return bRet;
    }

    ~Logger()
    {
        m_fhFile.Close();
    }
}