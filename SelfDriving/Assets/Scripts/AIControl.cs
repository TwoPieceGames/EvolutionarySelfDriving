﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AIControl : MonoBehaviour
{
    private bool bTeams = false;

    Trainer m_Trainer;
    NeuralNetwork lNN;

    protected float m_fAvgFitness;
    protected float m_fFitness;
    protected float fPassiveFitness;
    protected float fPassiveFitnessGain = 0.1f;
    protected float m_iAverageCount;
    protected float m_fPeakFitness = 0;

    protected float fFitnessTrainOffset = 8;

    protected int iAdaptiveTrainingInverval = 60;
    protected float fAdaptiveTrainingTimer = 0;

    public bool bAdapt = false;

    protected float fNearestEnemyDistance;
    protected List<string> m_sButtonsPressed;
    protected List<string> m_sButtonsToClear;
    protected Vector2 m_v2ControllerAxis;
    protected Vector2 m_v2SAxis;
    protected CarControl m_CC;

    protected InputInterface m_Input;

    protected Vector2 fSAxis;

    public GameObject goWButtonUI;
    public GameObject goSButtonUI;
    public GameObject goDButtonUI;
    public GameObject goAButtonUI;

    public Text txtGen;
    public Text txtSpecies;

    public Text txtOutput1;
    public Text txtOutput2;
    public Text txtOutput3;
    public Text txtOutput4;

    void Awake()
    {
        m_sButtonsPressed = new List<string>();
        m_sButtonsToClear = new List<string>();
        m_Trainer = GetComponent<Trainer>();
        m_Input = GetComponent<InputInterface>();
    }

    public List<string> GetButtonsPressed()
    {
        return m_sButtonsPressed;
    }

    public Vector2 GetAxisValues()
    {
        return m_v2ControllerAxis;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
       m_sButtonsPressed.Clear();
        if (m_Trainer)
        {
            lNN = m_Trainer.GetActiveNetwork();
        }

        lNN.Forward();
        List<string> sButtons = new List<string>();
        lNN.SetInputs(ref sButtons);
        m_Input.SetButtons(sButtons);
        //m_sButtonsPressed.Add(sButton);

        if (goWButtonUI)
        {
            if (sButtons.Contains("w")) 
                goWButtonUI.SetActive(true);
            else
                goWButtonUI.SetActive(false);

            if (sButtons.Contains("s"))
                goSButtonUI.SetActive(true);
            else
                goSButtonUI.SetActive(false);

            if (sButtons.Contains("a"))
                goAButtonUI.SetActive(true);
            else
                goAButtonUI.SetActive(false);

            if (sButtons.Contains("d"))
                goDButtonUI.SetActive(true);
            else
                goDButtonUI.SetActive(false);

            txtGen.text = lNN.GetGeneration().ToString();
            txtSpecies.text = lNN.GetSpecies().ToString();

            float[] fOutputs = lNN.GetOutputs();
            txtOutput1.text = fOutputs[0].ToString("0.0000");
            txtOutput2.text = fOutputs[1].ToString("0.0000");
            txtOutput3.text = fOutputs[2].ToString("0.0000");
            txtOutput4.text = fOutputs[3].ToString("0.0000");

        }

    }

    protected void UpdateFitness()
    {
        m_fFitness = 0;
        fPassiveFitness += fPassiveFitnessGain;
        m_fFitness += fPassiveFitness;

    } 
}