﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarControl : MonoBehaviour
{
    public Vector3 v3MoveDirection = Vector3.zero;
    InputInterface m_Input;
    CharacterController m_Controller;
    ObjectsTrigger m_ObjectsTrigger;
    PickupTrigger m_PickupTrigger;

    public float fTurnSpeed = 1;
    public float fAcceleration = 2;
    public float fBreakForce = 2;

    public float fFriction = 0.94f;

    public int iRewards = 0;

	// Use this for initialization
	void Start () {
        m_Input = GetComponent<InputInterface>();
        m_ObjectsTrigger = GetComponentInChildren<ObjectsTrigger>();
        m_PickupTrigger = GetComponentInChildren<PickupTrigger>();
    }

    public VisionHit GetQuadrantObject(int iQuadrant)
    {
        VisionHit vhRet = null;

        if (m_ObjectsTrigger.m_igoObjectsHit.ContainsKey(iQuadrant))
        {
            VisionHit go = m_ObjectsTrigger.m_igoObjectsHit[iQuadrant];
            vhRet = go;
        }

        return vhRet;
    }


    void ApplyFriction()
    {
        v3MoveDirection *= fFriction;
    }

    public void Reset()
    {
        iRewards = 0;
        m_PickupTrigger.Reset();
        v3MoveDirection = Vector3.zero;
    }

    // Update is called once per frame
    void Update ()
    {
        if (m_Input.GetButton("w"))
        {
            v3MoveDirection += transform.forward * fAcceleration;
        }

        if (m_Input.GetButton("a"))
        {
            //
            if (Vector3.Dot(v3MoveDirection, transform.forward) > 0)
            {
                transform.Rotate(new Vector3(0, fTurnSpeed * Time.deltaTime * v3MoveDirection.magnitude, 0));


                float fMag = v3MoveDirection.magnitude;
                v3MoveDirection = transform.forward * fMag;
            }
            else
            {
                transform.Rotate(new Vector3(0, -fTurnSpeed * Time.deltaTime * v3MoveDirection.magnitude, 0));


                float fMag = v3MoveDirection.magnitude;
                v3MoveDirection = transform.forward * -fMag;
            }

        }

        if (m_Input.GetButton("d"))
        {
            //
            if (Vector3.Dot(v3MoveDirection, transform.forward) > 0)
            {
                transform.Rotate(new Vector3(0, -fTurnSpeed * Time.deltaTime * v3MoveDirection.magnitude, 0));

                float fMag = v3MoveDirection.magnitude;
                v3MoveDirection = transform.forward * fMag;
            }
            else
            {
                transform.Rotate(new Vector3(0, fTurnSpeed * Time.deltaTime * v3MoveDirection.magnitude, 0));

                float fMag = v3MoveDirection.magnitude;
                v3MoveDirection = transform.forward * -fMag;
            }

        }

        if (m_Input.GetButton("s"))
        {
            v3MoveDirection -= transform.forward * fBreakForce;
        }

        ApplyFriction();
        this.transform.position += v3MoveDirection * Time.deltaTime;

	}
}
