﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReInforcementSet
{
    public float m_fInitialFitness;
    RecurrentNeuralNetwork m_RNNNetwork;
    public float[] m_fOutputs;
    public float[] m_fShortTermError;
    public float[] m_fLongTermError;

    public ReInforcementSet(float[] fIn, ref RecurrentNeuralNetwork network)
    {
        m_fOutputs = new float[fIn.Length];
        fIn.CopyTo(m_fOutputs, 0);
        m_RNNNetwork = network;
        m_fInitialFitness = m_RNNNetwork.GetFitness();
    }

    public void GetShortTermError(float fCurrentFitness)
    {
        m_RNNNetwork.Train(m_fOutputs, m_fInitialFitness, fCurrentFitness);
    }

    public void GetLongTermError(float fCurrentFitness)
    {
        m_RNNNetwork.Train(m_fOutputs, m_fInitialFitness, fCurrentFitness);
    }
}

public class Trainer : MonoBehaviour
{
    public bool bUseReinforcementLearning = false;
    private float fShortTermRewardInterval = 2.0f;
    private float fLongTermRewardInterval = 10f;
    private float fUpdateInterval = 0.5f;

    private float fIntervalTimer = 0;
    private bool bIntervalTick = false;

    private int iMaxArrayLength;
    private int iShortTermOffset;
    private int iLongTermOffset;

    private List<ReInforcementSet> m_ReInforcementSets = new List<ReInforcementSet>();

    private int iCurrentSpecies = 0;
    private int iCurrentGeneration = 0;
  
    private GameObject goPlayer;
    private AIControl m_AiControl;
    private CarControl m_CarControl;

    private float fDeltaFitness;
    private float fPreviousPeakFitness = 0;
    private int iPreviousFitnessMax = 8;
    //if we get stuck for more than generations, generate all new random

    private static int iPopulationSize = 20;
    private static int iSwapRate = 900; // 15 seconds
    private int iSwapTimer = 0;
    private int iTimeoutTimer = 0;
    private static int iTimeout = 600;

    private List<NeuralNetwork> m_NetworkList;
    private NeuralNetwork m_ActiveNetwork;
    private int m_iActiveNetworkIndex;
    private int m_iPlayerIndex;

    private float fPreviousGenerationPeakFitness = -5000;
    private float fPreviousGenerationAvgFitness = -5000;
    private static float fMaxStagnation = 10; //we didn't increase by more than 10? or we regressed? high mutation rate generation

    private int iStagnationCount;
    private static int iStagnationMax = 5;
    private static float fBaseMutationIncrease = 0.05f;
    private static float fBaseMutationRate = 0.1f;
    private float fMutationRate = fBaseMutationRate;

    private string sLogFile = "Training_";
    private string sLogDir = "C:\\Programs\\SelfDriving\\NetworkWeights\\";

    private string sNetworkWeightsDir = "C:\\Programs\\SelfDriving\\NetworkWeights\\";
    private string sWeightsCharacterDir = "Car";
    public string sInitWeightsFile = "";

    public int iMaxRecurrentSteps = 6;
    public int iMaxFrameDuration = 30;

    public int iTrainingInterval = 5;

    public float fLearningRate = 2;

    public bool bGRU = false;

    public int iWinningBonus = 50;

    Logger m_Logger;

    public Vector3 v3SpawnPosition = new Vector3(0, 0.5f, 0);
    public Text txtTimeoutTimer;

    public bool bUseRNN = false;

    void Start()
    {
        Application.runInBackground = true;
        Resolution r = Screen.currentResolution;
        if (r.refreshRate > 70)
            QualitySettings.vSyncCount = 2;
        else
            QualitySettings.vSyncCount = 1;

        Application.targetFrameRate = 60;

        System.DateTime dt = System.DateTime.Now;

        sLogFile += this.transform.name
            + "_Log_"
            + dt.Year.ToString() + "_"
            + dt.Month.ToString() + "_"
            + dt.Day.ToString()
            + "_" + dt.Hour
            + dt.Minute.ToString()
            + "_log.txt";

        goPlayer = this.transform.gameObject;
        m_NetworkList = new List<NeuralNetwork>();
        m_AiControl = goPlayer.GetComponent<AIControl>();
        m_CarControl = goPlayer.GetComponent<CarControl>();

        for (int i = 0; i < iPopulationSize; i++)
        {
            if (bUseRNN)
            {
                RecurrentNeuralNetwork lRNN = new RecurrentNeuralNetwork(iCurrentGeneration, iCurrentSpecies, m_CarControl, iMaxRecurrentSteps, iMaxFrameDuration);

                if (sInitWeightsFile != "")
                {
                    lRNN.InitWeights(null, sNetworkWeightsDir + sWeightsCharacterDir + "\\" + sInitWeightsFile, true);
                    //mutate these weights
                    if (i != 0)
                    {
                        lRNN.MutateWeights(0.1f, 0.8f);
                        lRNN.MutateRecurrentWeights(0.1f, 0.8f);
                    }
                }
                else
                {
                    lRNN.InitWeights();
                }

                lRNN.SetLearningRate(fLearningRate);
                lRNN.SetTrainingInterval(iTrainingInterval);
                m_NetworkList.Add(lRNN);
                iCurrentSpecies += 1;

            }
            else
            {
                NeuralNetwork lNN = new NeuralNetwork(iCurrentGeneration, iCurrentSpecies, m_CarControl, iMaxRecurrentSteps, iMaxFrameDuration);

                if (sInitWeightsFile != "")
                {
                    lNN.InitWeights(null, sNetworkWeightsDir + sWeightsCharacterDir + "\\" + sInitWeightsFile, true);
                    //mutate these weights
                    if (i != 0)
                    {
                        lNN.MutateWeights(0.1f, 0.8f);
                    }
                }
                else
                {
                    lNN.InitWeights();
                }

                lNN.SetLearningRate(fLearningRate);
                lNN.SetTrainingInterval(iTrainingInterval);
                m_NetworkList.Add(lNN);
                iCurrentSpecies += 1;
            }
        }

        m_ActiveNetwork = m_NetworkList[0];
        m_iActiveNetworkIndex = 0;
        UnityEngine.Random.InitState((int)System.DateTime.Now.Ticks);

        if (bUseReinforcementLearning)
        {
            iMaxArrayLength = (int)(fLongTermRewardInterval / fUpdateInterval);
            iShortTermOffset = (int)(fShortTermRewardInterval / fUpdateInterval);
            iLongTermOffset = (int)(fLongTermRewardInterval / fUpdateInterval);
        }

        m_Logger = new Logger(sLogDir, sLogFile);
        PickupTrigger.OnPlayerPickup += OnPickup;
    }

    void OnPickup()
    {
        iTimeoutTimer = 0;
    }

    public void NextSpecies(bool bWon = false)
    {
        iTimeoutTimer = 0;
        if (m_iActiveNetworkIndex != iPopulationSize - 1)
        {
            m_iActiveNetworkIndex += 1;
            m_ActiveNetwork = m_NetworkList[m_iActiveNetworkIndex];
            iTimeoutTimer = 0;

            this.transform.position = v3SpawnPosition;
            this.transform.rotation = Quaternion.Euler(Vector3.zero);
            m_CarControl.Reset();
            //tell gamedata to reset us?
        }
        else
        {
            iCurrentGeneration += 1;
            iCurrentSpecies = 0;
  
            SortNetworks(true, true, true);

            UnityEngine.Random.InitState((int)System.DateTime.Now.Ticks + (int)m_NetworkList[0].GetFitness());
            //spawn 4 new species, 2 from the top 1-2 and 2 from 2-3
            float fPeak = 0;
            float fPeakAvg = -9999;
            int iPeakIndex = 0;
            int iPeakAvgIndex = 0;
            for (int i = 0; i < m_NetworkList.Count; i++)
            {
                if (m_NetworkList[i].GetPeakFitness() > fPeak)
                {
                    fPeak = m_NetworkList[i].GetPeakFitness();
                    iPeakIndex = i;
                }
                if (m_NetworkList[i].GetAverageFitness() > fPeakAvg)
                {
                    fPeakAvg = m_NetworkList[i].GetAverageFitness();
                    iPeakAvgIndex = i;
                }
            }

            //we just added iPopulation size, so we have a list of size 2 * ipopulationSize, trim the old weights out.
            float fMax = m_NetworkList[0].GetPeakFitness() + m_NetworkList[0].GetAverageFitness();
            string sText = "peak fitness:" +
                fMax.ToString() +
                " generation: " +
                m_NetworkList[iPeakIndex].GetGeneration() +
                " species:" + m_NetworkList[iPeakIndex].GetSpecies() + "\n";
            m_Logger.Write(sText);

            PrintNetworks();

            m_NetworkList[0].SetSpecies(iPopulationSize + 1);
            int iNumNetworks = m_NetworkList.Count;

            if ((fPeak - fPreviousGenerationPeakFitness < 10) && (fPeakAvg - fPreviousGenerationAvgFitness < 10))
            {
                fMutationRate += fBaseMutationIncrease;
                if (fMutationRate > 1)
                {
                    fMutationRate = 1;
                }
                if (iStagnationCount > iStagnationMax)
                {
                    UnityEngine.Random.InitState((int)System.DateTime.Now.Ticks + (int)m_NetworkList[0].GetFitness());
                    GenerateNextGeneration(fMutationRate * 3, 1f, 2);
                    m_Logger.Write("Stagnation Detected, using triple mutation rate: " + fMutationRate.ToString() + "\n");
                    fMutationRate = fBaseMutationRate;
                }
                else
                {
                    GenerateNextGeneration(fMutationRate, 0.6f, 2);
                    iStagnationCount += 1;
                }
            }
            else
            {
                iStagnationCount = 0;
                fMutationRate = fBaseMutationRate;
                GenerateNextGeneration(fMutationRate, 0.5f);
            }
            fPreviousGenerationPeakFitness = fPeak;
            fPreviousGenerationAvgFitness = fPeakAvg;

            NeuralNetwork lPreviousBest = m_NetworkList[0];

            m_NetworkList.RemoveRange(0, iNumNetworks);

            m_NetworkList.Add(lPreviousBest);
      
            iCurrentSpecies = 0;
            iTimeoutTimer = 0;
            m_iActiveNetworkIndex = 0;
            m_ActiveNetwork = m_NetworkList[m_iActiveNetworkIndex];

            this.transform.position = v3SpawnPosition;
            this.transform.rotation = Quaternion.Euler(Vector3.zero);
            m_CarControl.Reset();
        }

    }

    private void GenerateNextGeneration(float fMutationRate, float fStepSize, int iCrossoverType = 0)
    {
        for (int i = 0; i < iPopulationSize; i++)
        {
            float fMaxFitness = m_NetworkList[0].GetAverageFitness();
            float fMaxPeak = m_NetworkList[0].GetPeakFitness();
            float fSecondPlaceFitness = m_NetworkList[1].GetAverageFitness();
            float fMaxPeak2 = m_NetworkList[1].GetPeakFitness();

            //get crossover probability based on how well they did in comparison
            float fProb = 1 - (fMaxFitness + fMaxPeak / (fSecondPlaceFitness + fMaxPeak + fMaxFitness + fMaxPeak2));


            UnityEngine.Random.InitState((int)System.DateTime.Now.Ticks + (int)m_NetworkList[0].GetFitness());

            if (bUseRNN)
            {
                RecurrentNeuralNetwork lRNN = new RecurrentNeuralNetwork(iCurrentGeneration, iCurrentSpecies, m_CarControl, iMaxRecurrentSteps, iMaxFrameDuration);
                lRNN.m_Weights = NeuralNetworkUtils.CrossoverWeightsProbability(
                    m_NetworkList[0].m_Weights,
                    m_NetworkList[1].m_Weights,
                    fProb);
                lRNN.SetRecurrentWeights(NeuralNetworkUtils.CrossoverWeightsProbability(
                    m_NetworkList[0].m_RecurrentWeights,
                    m_NetworkList[1].m_RecurrentWeights,
                    fProb));
                /*
                lRNN.SetResetWeights(NeuralNetworkUtils.CrossoverWeightsProbability(
                    m_NetworkList[0].m_ResetWeights,
                    m_NetworkList[1].m_ResetWeights,
                    fProb));
                lRNN.SetUpdateWeights(NeuralNetworkUtils.CrossoverWeightsProbability(
                    m_NetworkList[0].m_UpdateWeights,
                    m_NetworkList[1].m_UpdateWeights,
                    fProb));
                    */
                lRNN.MutateWeights(fMutationRate, fStepSize);
                lRNN.MutateRecurrentWeights(fMutationRate, fStepSize);
                /*
                lRNN.MutateResetWeights(fMutationRate, fStepSize);
                lRNN.MutateUpdateWeights(fMutationRate, fStepSize);
                */
                lRNN.WriteData("", lRNN.m_RecurrentWeights);
                lRNN.SetDiversity(ref m_NetworkList[0].m_Weights);
                lRNN.SetLearningRate(fLearningRate);
                lRNN.SetTrainingInterval(iTrainingInterval);
                m_NetworkList.Add(lRNN);
            }
            else
            {
                NeuralNetwork lRNN = new NeuralNetwork(iCurrentGeneration, iCurrentSpecies, m_CarControl, iMaxRecurrentSteps, iMaxFrameDuration);
                lRNN.m_Weights = NeuralNetworkUtils.CrossoverWeightsProbability(
                    m_NetworkList[0].m_Weights,
                    m_NetworkList[1].m_Weights,
                    fProb);
    
                /*
                lRNN.SetResetWeights(NeuralNetworkUtils.CrossoverWeightsProbability(
                    m_NetworkList[0].m_ResetWeights,
                    m_NetworkList[1].m_ResetWeights,
                    fProb));
                lRNN.SetUpdateWeights(NeuralNetworkUtils.CrossoverWeightsProbability(
                    m_NetworkList[0].m_UpdateWeights,
                    m_NetworkList[1].m_UpdateWeights,
                    fProb));
                    */
                lRNN.MutateWeights(fMutationRate, fStepSize);
                /*
                lRNN.MutateResetWeights(fMutationRate, fStepSize);
                lRNN.MutateUpdateWeights(fMutationRate, fStepSize);
                */
                lRNN.WriteData("");
                lRNN.SetDiversity(ref m_NetworkList[0].m_Weights);
                lRNN.SetLearningRate(fLearningRate);
                lRNN.SetTrainingInterval(iTrainingInterval);
                m_NetworkList.Add(lRNN);
            }

            iCurrentSpecies += 1;
        }
        /*
        for (int j = iPopulationSize / 2; j < iPopulationSize; j++)
        {
            UnityEngine.Random.seed = (int)System.DateTime.Now.Ticks + (int)m_NetworkList[1].GetFitness();
            RecurrentNeuralNetwork lRNN = new RecurrentNeuralNetwork(iCurrentGeneration, iCurrentSpecies, m_Move, iMaxRecurrentSteps, iMaxFrameDuration);
            lRNN.m_Weights = NeuralNetworkUtils.CrossoverWeightsDoubleDivision(
                m_NetworkList[1].m_Weights,
                m_NetworkList[2].m_Weights);
            lRNN.SetRecurrentWeights(NeuralNetworkUtils.CrossoverWeightsDoubleDivision(
                m_NetworkList[1].m_RecurrentWeights,
                m_NetworkList[2].m_RecurrentWeights));
            lRNN.SetResetWeights(NeuralNetworkUtils.CrossoverWeightsDoubleDivision(
                m_NetworkList[1].m_ResetWeights,
                m_NetworkList[2].m_ResetWeights));
            lRNN.SetUpdateWeights(NeuralNetworkUtils.CrossoverWeightsDoubleDivision(
                m_NetworkList[1].m_UpdateWeights,
                m_NetworkList[2].m_UpdateWeights));
            lRNN.MutateWeights(fMutationRate, fStepSize);
            lRNN.MutateRecurrentWeights(fMutationRate + 0.1f, fStepSize + 0.2f);
            lRNN.MutateResetWeights(fMutationRate, fStepSize);
            lRNN.MutateUpdateWeights(fMutationRate, fStepSize);
            lRNN.WriteData("",lRNN.m_RecurrentWeights);
            lRNN.SetDiversity(ref m_NetworkList[1].m_Weights);
            lRNN.SetLearningRate(fLearningRate);
            lRNN.SetTrainingInterval(iTrainingInterval);
            m_NetworkList.Add(lRNN);
            iCurrentSpecies += 1;
        }
        */
    }

    // Update is called once per frame
    void Update()
    {
        fIntervalTimer += Time.deltaTime;

        if (m_ActiveNetwork.GetFitness() < 0 || iTimeoutTimer > iTimeout)
        {
            // Next generation
            if (!bUseReinforcementLearning)
                NextSpecies();


            //check to make sure we tried all the children
            /*
            for (int i = m_iActiveNetworkIndex;i < m_NetworkList.Count; i++)
            {
                if (m_NetworkList[i].GetAverageFitness() == 0)
                {
                    m_iActiveNetworkIndex = i;
                    m_ActiveNetwork = m_NetworkList[i];
                    iTimeoutTimer = 0;
                    m_Move.iNumStocks = 4;
                    iSwapTimer = 0;
                    return;
                }
            }
            */

        }

        /*
        if (bUseReinforcementLearning)
        {
            if (fIntervalTimer > fUpdateInterval)
            {
                fIntervalTimer -= fUpdateInterval;
                //update

                //create a new reinforcement set
                ReInforcementSet reinforcementSet = new ReInforcementSet(m_ActiveNetwork.GetCurrentOutputs(), ref m_ActiveNetwork);

                //add it to the list
                m_ReInforcementSets.Add(reinforcementSet);

                if (m_ReInforcementSets.Count > iShortTermOffset)
                {
                    //update short term of next set
                    ReInforcementSet r = m_ReInforcementSets[m_ReInforcementSets.Count - iShortTermOffset];
                    r.GetShortTermError(m_ActiveNetwork.GetFitness());
                }

                if (m_ReInforcementSets.Count > iLongTermOffset)
                {
                    //update long term set
                    ReInforcementSet r = m_ReInforcementSets[m_ReInforcementSets.Count - iLongTermOffset];
                    r.GetLongTermError(m_ActiveNetwork.GetFitness());

                    //remove the first element in the list
                    m_ReInforcementSets.RemoveAt(0);
                }


            }
        }
        */

        iTimeoutTimer += 1;
        txtTimeoutTimer.text = iTimeoutTimer.ToString();

        /*
        iSwapTimer += 1;
        if (iSwapTimer > iSwapRate)
        {
            iSwapTimer = 0;
            m_iActiveNetworkIndex += 1;
            m_iActiveNetworkIndex %= m_NetworkList.Count;
            m_ActiveNetwork = m_NetworkList[m_iActiveNetworkIndex];
            //Look into doing simulated annealing every 10 timesteps
        }
        */

    }

    public NeuralNetwork GetActiveNetwork()
    {
        return m_ActiveNetwork;
    }

    private void SortNetworks(bool bHighestFit, bool bHighestAvgFit, bool bHighestDiversity)
    {
        Dictionary<int, int[]> iRanks = new Dictionary<int, int[]>();
        for (int i = 0; i < m_NetworkList.Count; i++)
        {
            iRanks.Add(m_NetworkList[i].GetSpecies(), new int[3]);
        }

        NeuralNetwork NNHolder;

        // Peak Fitness
        if (bHighestFit)
        {

            for (int i = 1; i < m_NetworkList.Count; i++)
            {
                int j = i - 1;
                NNHolder = m_NetworkList[i];
                while (j >= 0 && m_NetworkList[j].GetPeakFitness() < NNHolder.GetPeakFitness())
                {
                    m_NetworkList[j + 1] = m_NetworkList[j];
                    j--;
                }
                m_NetworkList[j + 1] = NNHolder;
            }

            for (int j = 0; j < m_NetworkList.Count; j++)
            {
                iRanks[m_NetworkList[j].GetSpecies()][0] = j;
            }
        }

        // Avg Fitness
        if (bHighestAvgFit)
        {
            for (int i = 1; i < m_NetworkList.Count; i++)
            {
                int j = i - 1;
                NNHolder = m_NetworkList[i];
                while (j >= 0 && m_NetworkList[j].GetAverageFitness() < NNHolder.GetAverageFitness())
                {
                    m_NetworkList[j + 1] = m_NetworkList[j];
                    j--;
                }
                m_NetworkList[j + 1] = NNHolder;
            }

            for (int j = 0; j < m_NetworkList.Count; j++)
            {
                iRanks[m_NetworkList[j].GetSpecies()][1] = j;
            }
        }

        // Diversity
        if (bHighestDiversity)
        {
            for (int i = 1; i < m_NetworkList.Count; i++)
            {
                int j = i - 1;
                NNHolder = m_NetworkList[i];
                while (j >= 0 && m_NetworkList[j].GetDiversity() < NNHolder.GetDiversity())
                {
                    m_NetworkList[j + 1] = m_NetworkList[j];
                    j--;
                }
                m_NetworkList[j + 1] = NNHolder;
            }

            for (int j = 0; j < m_NetworkList.Count; j++)
            {
                iRanks[m_NetworkList[j].GetSpecies()][2] = j + 13;
            }
        }

        // Final Sorting
        int[] iSums = new int[m_NetworkList.Count];
        for (int i = 0; i < m_NetworkList.Count; i++)
        {
            iSums[i] = 0;
            for (int j = 0; j < 3; j++)
            {
                iSums[i] += iRanks[m_NetworkList[i].GetSpecies()][j];
            }
        }

        // Sort by lowest
        int iSumHolder = 0;
        for (int i = 1; i < m_NetworkList.Count; i++)
        {
            int j = i - 1;
            NNHolder = m_NetworkList[i];
            iSumHolder = iSums[i];
            while (j >= 0 && iSums[j] > iSumHolder)
            {
                m_NetworkList[j + 1] = m_NetworkList[j];
                iSums[j + 1] = iSums[j];
                j--;
            }
            m_NetworkList[j + 1] = NNHolder;
            iSums[j + 1] = iSumHolder;
        }

    }

    void PrintNetworks()
    {
        string sText = "Final Order:  Avg Fitness | Peak Fitness | Diversity \n";

        for (int i = 0; i < m_NetworkList.Count; i++)
        {
            sText += " S: " + m_NetworkList[i].GetSpecies().ToString() + "    "
                + i.ToString()
                + ":  " + m_NetworkList[i].GetAverageFitness().ToString()
                + "   |   "
                + m_NetworkList[i].GetPeakFitness().ToString()
                + "   |   "
                + m_NetworkList[i].GetDiversity().ToString();

            sText += "\n";
        }

        m_Logger.Write(sText);
    }
}