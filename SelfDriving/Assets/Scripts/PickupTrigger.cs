﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupTrigger : MonoBehaviour
{
    private Trainer m_Trainer;
    private CarControl m_CarControl;

    int iPickUpId = 0;

    public delegate void PickupAction();
    public static event PickupAction OnPlayerPickup;

    List<GameObject> m_goHitObjectsThisFrame = new List<GameObject>();
    List<GameObject> m_goDeactivatedObjects = new List<GameObject>();
	// Use this for initialization
	void Start () {
        m_Trainer = transform.root.GetComponent<Trainer>();
        m_CarControl = transform.root.GetComponent<CarControl>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        m_goHitObjectsThisFrame.Clear();

    }

    public void Reset()
    {
        iPickUpId = 0;
        for (int i = 0; i < m_goDeactivatedObjects.Count; i++)
        {
            m_goDeactivatedObjects[i].SetActive(true);
        }

        m_goDeactivatedObjects.Clear();
    }

    private void OnTriggerEnter(Collider other)
    {
        GameObject goBase = other.transform.root.gameObject;
        if (!m_goHitObjectsThisFrame.Contains(goBase))
        {
            m_goHitObjectsThisFrame.Add(goBase);
            if (goBase.GetComponent<TargetTrigger>())
            {
                if (goBase.GetComponent<TargetTrigger>().iIndex != iPickUpId)
                {
                    //punish
                    print("Car Hit Incorrect Pickup: " + goBase.name);
                    m_CarControl.iRewards = -20;
                    
                }
                else
                {
                    //reward
                    print("Car Hit Reward: " + goBase.name);
                    m_CarControl.iRewards += goBase.GetComponent<TargetTrigger>().iReward;
                    iPickUpId += 1;
                    goBase.SetActive(false);
                    m_goDeactivatedObjects.Add(goBase);

                    if (OnPlayerPickup != null)
                    {
                        OnPlayerPickup();
                    }
                }
            }
            else
            {
                print("Car Hit Obstacle: " + goBase.name);
                m_CarControl.iRewards = -80;
            }
        }
        
    }
}
