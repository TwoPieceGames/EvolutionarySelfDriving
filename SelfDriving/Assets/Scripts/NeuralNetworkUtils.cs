﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;
using System.IO;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

[Serializable]
public class NetworkStorageData
{
    public int iGeneration;
    public int iSpecies;
    public List<List<List<float>>> fWeights;
    public List<List<float>> fRecurWeights;
    public List<List<float>> fUpdateWeights;
    public List<List<float>> fResetWeights;

    public NetworkStorageData(int generation, int species, float[][][] weights, float[][] recurrentWeights = null)
    {
        fWeights = new List<List<List<float>>>(weights.Length);
        iGeneration = generation;
        iSpecies = species;
        for (int i = 0; i < weights.Length; i++)
        {
            fWeights.Add(new List<List<float>>(weights[i].Length));
            for (int j = 0; j < weights[i].Length; j++)
            {
                fWeights[i].Add(new List<float>(weights[i][j].Length));
                for (int k = 0; k < weights[i][j].Length; k++)
                {
                    fWeights[i][j].Add(weights[i][j][k]);
                }
            }
        }
        if (recurrentWeights != null)
        {
            fRecurWeights = new List<List<float>>(recurrentWeights.Length);
            for (int i = 0; i < recurrentWeights.Length; i++)
            {
                fRecurWeights.Add(new List<float>(recurrentWeights[i].Length));
                for (int j = 0; j < recurrentWeights[i].Length; j++)
                {
                    fRecurWeights[i].Add(recurrentWeights[i][j]);
                }
            }
        }
    }

    public NetworkStorageData()
    {
    }
}
public static class NeuralNetworkUtils
{
    public static float[][][] ConvertStorageWeights(List<List<List<float>>> fWeights)
    {
        float[][][] fRet = new float[fWeights.Count][][];
        for (int i = 0; i < fRet.Length; i++)
        {
            fRet[i] = new float[fWeights[i].Count][];
            for (int j = 0; j < fRet[i].Length; j++)
            {
                fRet[i][j] = new float[fWeights[i][j].Count];
                for (int k = 0; k < fRet[i][j].Length; k++)
                {
                    fRet[i][j][k] = fWeights[i][j][k];
                }
            }
        }

        return fRet;
    }

    public static float[][] ConvertStorageRecurrentWeights(List<List<float>> fWeights)
    {
        float[][] fRet = new float[fWeights.Count][];
        for (int i = 0; i < fRet.Length; i++)
        {
            fRet[i] = new float[fWeights[i].Count];
            for (int j = 0; j < fRet[i].Length; j++)
            {
                fRet[i][j] = fWeights[i][j];
            }
        }

        return fRet;
    }

    public static float Sigmoid(float fZ)
    {
        return 1 / (1 + Mathf.Exp(-fZ));
    }

    public static float SoftMax(float fZ, float[] fOutputs)
    {
        float fRet = 0;

        float fNum = Mathf.Exp(fZ);
        float fDenom = 0;

        for (int i = 0; i < fOutputs.Length; i++)
        {
            if (fOutputs[i] == fZ)
            {
                continue;
            }
            fDenom += Mathf.Exp(fOutputs[i]);
        }

        fRet = fNum / fDenom;

        return fRet;
    }

    public static float SoftMaxPrime(float fZ, float[] fOutputs)
    {
        float fRet = 0;

        fRet = SoftMax(fZ, fOutputs);
        fRet = fRet * (1 - fRet);

        return fRet;
    }

    public static float TanH(float fZ)
    {
        float fRet = 0;

        fRet = (2 * Sigmoid(fZ)) - 1;

        return fRet;
    }

    public static float TanHPrime(float fZ)
    {
        return 1 - Mathf.Pow(TanH(fZ), 2);
    }

    public static float MinMax(float x, float max, float min, float dNewMax, float dNewMin)
    {
        if (x > max)
            x = max;
        else if (x < min)
            x = min;

        x = (x - min) * (dNewMax - dNewMin) / (max - min) + dNewMin;
        return x;
    }

    public static Vector2 UnitCircleAdjustedValue(float fInputA, float fInputB, float fRadius)
    {
        Vector2 v2Ret = new Vector2(fInputA, fInputB);

        if (v2Ret.magnitude > 1)
        {
            if (fInputA > fInputB)
            {
                //solve for b
                v2Ret.y = Mathf.Sqrt(Mathf.Pow(fRadius, 2) - Mathf.Pow(fInputA, 2));
            }
            else
            {
                //solve for a
                v2Ret.x = Mathf.Sqrt(Mathf.Pow(fRadius, 2) - Mathf.Pow(fInputB, 2));
            }
        }

        return v2Ret;
    }

    public static Vector2 NormalizePosition(Vector3 v3Pos)
    {
        Vector2 v2Ret = Vector2.zero;
        v2Ret.x = MinMax(v3Pos.z, 25, -25, 10, -10);
        v2Ret.y = MinMax(v3Pos.y, 25, -25, 10, -10);
        return v2Ret;
    }

    public static Vector2 NormalizeVelocity(Vector3 v3MoveDirection)
    {
        Vector2 v2Ret = Vector2.zero;
        v2Ret.x = MinMax(v3MoveDirection.z, 60, -60, -10, 10);
        v2Ret.y = MinMax(v3MoveDirection.y, 60, -60, -10, 10);
        return v2Ret;
    }

    public static float NormalizePercentage(float fPercentage)
    {
        float fRet = 0;
        fRet = MinMax(fPercentage, 250, 0, -10, 10);
        return fRet;
    }

    public static float NextGausiannFlot()
    {
        float U, u, v, S;

        do
        {
            u = 2.0f * UnityEngine.Random.value - 1.0f;
            v = 2.0f * UnityEngine.Random.value - 1.0f;
            S = u * u + v * v;
        }
        while (S >= 1.0);

        float fac = Mathf.Sqrt(-2.0f * Mathf.Log(S) / S);
        return u * fac;
    }

    public static void WriteWeights(string sFilename, float[][][] fWeights, float[][] fRecurWeights, int generation, int species, float fSeed)
    {
        
        NetworkStorageData obj = new NetworkStorageData(generation, species, fWeights, fRecurWeights);
        System.IO.StreamWriter file = new System.IO.StreamWriter(sFilename);
        string sJson = JsonConvert.SerializeObject(obj) + "\n";
        file.Write(sJson);
        file.Close();
    }

    public static List<NetworkStorageData> ReadLinesIntoObjects(string sFilename)
    {
        List<NetworkStorageData> dataRet = new List<NetworkStorageData>();
        string[] lines = System.IO.File.ReadAllLines(sFilename);
        for (int i = 0; i < lines.Length; i++)
        {
            NetworkStorageData newData = JsonConvert.DeserializeObject<NetworkStorageData>(@lines[0]);
            dataRet.Add(newData);
        }
        return dataRet;
        
    }

    public static void MutateWeights(ref float[][][] fWeights, float fMutationRate, float fStepSize /* = 1 - 4*/)
    {
        //seed
        UnityEngine.Random.seed = UnityEngine.Random.Range(0, 4000);
        for (int i = 0; i < fWeights.Length; i++)
        {
            for (int j = 0; j < fWeights[i].Length; j++)
            {
                for (int k = 0; k < fWeights[i][j].Length; k++)
                {
                    float fRand = UnityEngine.Random.Range(0f, 1f);
                    if (fRand < fMutationRate)
                    {
                        fWeights[i][j][k] = UnityEngine.Random.Range(-1f, 1f);
                    }
                }
            }
        }
    }

    public static void MutateWeights(ref float[][] fWeights, float fMutationRate, float fStepSize /* = 0 - 2*/)
    {
        //seed
        UnityEngine.Random.seed = UnityEngine.Random.Range(0, 4000);
        for (int i = 0; i < fWeights.Length; i++)
        {
            for (int k = 0; k < fWeights[i].Length; k++)
            {
                float fRand = UnityEngine.Random.Range(0f, 1f);
                if (fRand < fMutationRate)
                {
                    float fDif = UnityEngine.Random.Range(-1f, 1f);
                    fWeights[i][k] *= fDif;
                }
            }
        }
    }

    public static List<float[][][]> GenerateNextGeneration(ref List<float[][][]> fBestWeights, int iGeneration, int iNumChildren, int iTwoPoint)
    {
        List<float[][][]> fNextGenRet = new List<float[][][]>();



        return fNextGenRet;
    }

    public static float[][][] CrossoverWeightsSingleDivision(ref float[][][] fWeights1, ref float[][][] fWeights2)
    {
        float[][][] fWeightRet = new float[fWeights1.Length][][];
        bool bTakeFirst = true;
        int iMax = fWeights1.Length * fWeights1[0].Length * fWeights1[0][0].Length;
        int iCrossoverPoint = UnityEngine.Random.Range(0, iMax);

        for (int i = 0; i < fWeights1.Length; i++)
        {
            fWeightRet[i] = new float[fWeights1[i].Length][];
            for (int j = 0; j < fWeights1[i].Length; j++)
            {
                fWeightRet[i][j] = new float[fWeights1[i][j].Length];
                for (int k = 0; k < fWeights1[i][j].Length; k++)
                {
                    if (i > iCrossoverPoint)
                    {
                        bTakeFirst = false;
                    }

                    if (bTakeFirst)
                    {
                        fWeightRet[i][j][k] = fWeights1[i][j][k];
                    }
                    else
                    {
                        fWeightRet[i][j][k] = fWeights2[i][j][k];
                    }
                }
            }
        }

        return fWeightRet;
    }

    public static float[][][] CrossoverWeightsDoubleDivision(float[][][] fWeights1, float[][][] fWeights2)
    {
        float[][][] fWeightRet = new float[fWeights1.Length][][];
        bool bTakeFirst = true;
        int iMax = fWeights1.Length * fWeights1[0].Length * fWeights1[0][0].Length;
        int iCrossoverPoint1 = UnityEngine.Random.Range(0, iMax);
        int iCrossoverPoint2 = UnityEngine.Random.Range(0, iMax);

        for (int i = 0; i < fWeights1.Length; i++)
        {
            fWeightRet[i] = new float[fWeights1[i].Length][];
            for (int j = 0; j < fWeights1[i].Length; j++)
            {
                fWeightRet[i][j] = new float[fWeights1[i][j].Length];
                for (int k = 0; k < fWeights1[i][j].Length; k++)
                {
                    if (i > iCrossoverPoint1 && i < iCrossoverPoint2)
                    {
                        bTakeFirst = false;
                    }
                    else
                    {
                        bTakeFirst = true;
                    }

                    if (bTakeFirst)
                    {
                        fWeightRet[i][j][k] = fWeights1[i][j][k];
                    }
                    else
                    {
                        fWeightRet[i][j][k] = fWeights2[i][j][k];
                    }
                }
            }
        }

        return fWeightRet;
    }

    public static float[][][] CrossoverWeightsProbability(float[][][] fWeights1, float[][][] fWeights2, float fChanceToTakeSecond)
    {
        float[][][] fWeightRet = new float[fWeights1.Length][][];

        for (int i = 0; i < fWeights1.Length; i++)
        {
            fWeightRet[i] = new float[fWeights1[i].Length][];
            for (int j = 0; j < fWeights1[i].Length; j++)
            {
                fWeightRet[i][j] = new float[fWeights1[i][j].Length];
                for (int k = 0; k < fWeights1[i][j].Length; k++)
                {
                    float fRand = UnityEngine.Random.Range(0f, 1f);
                    if (fRand > fChanceToTakeSecond)
                    {
                        fWeightRet[i][j][k] = fWeights1[i][j][k];
                    }
                    else
                    {
                        fWeightRet[i][j][k] = fWeights2[i][j][k];
                    }
                }
            }
        }

        return fWeightRet;
    }

    public static float[][] CrossoverWeightsSingleDivision(float[][] fWeights1, float[][] fWeights2)
    {
        float[][] fWeightRet = new float[fWeights1.Length][];
        bool bTakeFirst = true;
        int iMax = fWeights1.Length * fWeights1[0].Length;
        int iCrossoverPoint = UnityEngine.Random.Range(0, iMax);

        for (int i = 0; i < fWeights1.Length; i++)
        {
            fWeightRet[i] = new float[fWeights1[i].Length];
            for (int k = 0; k < fWeights1[i].Length; k++)
            {

                if (i > iCrossoverPoint)
                {
                    bTakeFirst = false;
                }

                if (bTakeFirst)
                {
                    fWeightRet[i][k] = fWeights1[i][k];
                }
                else
                {
                    fWeightRet[i][k] = fWeights2[i][k];
                }
            }
        }

        return fWeightRet;
    }

    public static float[][] CrossoverWeightsDoubleDivision(float[][] fWeights1, float[][] fWeights2)
    {
        float[][] fWeightRet = new float[fWeights1.Length][];

        bool bTakeFirst = true;
        int iMax = fWeights1.Length * fWeights1[0].Length;
        int iCrossoverPoint1 = UnityEngine.Random.Range(0, iMax);
        int iCrossoverPoint2 = UnityEngine.Random.Range(0, iMax);

        for (int i = 0; i < fWeights1.Length; i++)
        {
            fWeightRet[i] = new float[fWeights1[i].Length];
            for (int k = 0; k < fWeights1[i].Length; k++)
            {
                if (i > iCrossoverPoint1 && i < iCrossoverPoint2)
                {
                    bTakeFirst = false;
                }
                else
                {
                    bTakeFirst = true;
                }

                if (bTakeFirst)
                {
                    fWeightRet[i][k] = fWeights1[i][k];
                }
                else
                {
                    fWeightRet[i][k] = fWeights2[i][k];
                }
            }
        }

        return fWeightRet;
    }

    public static float[][] CrossoverWeightsProbability(float[][] fWeights1, float[][] fWeights2, float fChanceToTakeSecond)
    {
        float[][] fWeightRet = new float[fWeights1.Length][];

        for (int i = 0; i < fWeights1.Length; i++)
        {
            fWeightRet[i] = new float[fWeights1[i].Length];
            for (int k = 0; k < fWeights1[i].Length; k++)
            {
                float fRand = UnityEngine.Random.Range(0f, 1f);
                if (fRand > fChanceToTakeSecond)
                {
                    fWeightRet[i][k] = fWeights1[i][k];
                }
                else
                {
                    fWeightRet[i][k] = fWeights2[i][k];
                }
            }
        }

        return fWeightRet;
    }

}
